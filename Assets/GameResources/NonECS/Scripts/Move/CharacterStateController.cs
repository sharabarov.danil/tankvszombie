using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Character State Controller
    /// </summary>
    public class CharacterStateController : MonoBehaviour {

        [Header("Target Tag:")]
        [SerializeField]
        private string _targetTag = "Player";

        [Header("NavigationData:")]
        [SerializeField]
        private NavigationData _navigationData;

        private HealthController _healthController;
        private GameObject _target = null;
        private List<CharacterState> _characterStates;

        private void Awake() {
            _characterStates = GetComponents<CharacterState>().ToList();
            _healthController = GetComponent<HealthController>();
        }

        private void OnEnable() {
            _characterStates.ForEach(x => x.Init());
        }

        private void Update() {
            ChangeState();
        }

        private void ChangeState() {

            if (_healthController.IsDead) {
                _characterStates.ForEach(x => x.Die());
                return;
            }

            Collider[] hitColliders = Physics.OverlapSphere(transform.position, _navigationData.DistanceLimits.y);

            if (hitColliders.Length > 0) {
                foreach (var collider in hitColliders) {
                    if (collider != null && collider.gameObject.tag == _targetTag) {
                        _target = collider.gameObject;
                        break;
                    }
                }
            } else {
                _target = null;
            }

            if (_target != null) {
                if (Vector3.Distance(transform.position, _target.transform.position) > _navigationData.DistanceLimits.x) {
                    _characterStates.ForEach(x => x.Move(_target));
                } else {
                    _characterStates.ForEach(x => x.Hit(_target));
                }
            } else {
                _characterStates.ForEach(x => x.Idle());
            }
        }
    }
}
