﻿using UnityEngine;

namespace TankVsZombie.NoECS {
    /// <summary>
    /// Speed data
    /// </summary>

    [CreateAssetMenu(fileName = "NavigationData", menuName = "GameData/NoECS/Create NavigationData")]
    public class NavigationData : ScriptableObject {
        [Header("Distance:")]
        [SerializeField]
        private Vector2 _distanceLimits = new Vector2(3f, 20f);

        public Vector2 DistanceLimits {
            get {
                return _distanceLimits;
            }
        }
    }
}
