using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Character move ontroller
    /// </summary>
    public class CharacterMoveController : AbstractMoveController {

        [Header("Speed Data:")]
        [SerializeField]
        protected SpeedData _speedData = null;

        private InputData _inputData;
        private CharacterController _characterController = null;

        private void Awake() {
            _inputData = new PlayerInputData();
            _characterController = GetComponent<CharacterController>();
        }

        private void OnEnable() {
            _inputData.Init();
        }

        private void OnDisable() {
            _inputData.Dispose();
        }

        private void FixedUpdate() {
            Move(_inputData.Move);
        }

        protected override void Move(Vector2 direction) {
            Vector3 inputDirection = new Vector3(direction.x, 0, direction.y);

            if (inputDirection.magnitude > 0.1f) {

                var rotation = Quaternion.LookRotation(inputDirection);

                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _turnSpeedData.Speed);

                _characterController.Move(new Vector3(direction.x, 0, direction.y) * _speedData.Speed * Time.deltaTime);
            }
        }
    }
}
