﻿using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Move Controller
    /// </summary>

    public abstract class AbstractMoveController : MonoBehaviour {

        [Header("Turn Speed Data:")]
        [SerializeField]
        protected SpeedData _turnSpeedData = null;

        /// <summary>
        /// Move
        /// </summary>
        /// <param name="directionY"></param>

        protected abstract void Move(Vector2 direction);

    }
}
