using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Character State
    /// </summary>
    public abstract class CharacterState : MonoBehaviour {
        public abstract void Init();
        public abstract void Move(GameObject target);

        public abstract void Idle();

        public abstract void Hit(GameObject target);

        public abstract void Die();
    }
}
