﻿using UnityEngine;
using UnityEngine.AI;

namespace TankVsZombie.NoECS {
    /// <summary>
    /// Navigation Controller
    /// </summary>
    [RequireComponent(typeof(NavMeshAgent))]
    public class NavigationController : CharacterState {

        [Header("Speed Data:")]
        [SerializeField]
        private SpeedData _speedData = null;

        [Header("Turn Speed Data:")]
        [SerializeField]
        private SpeedData _turnSpeedData = null;

        private GameObject _target = null;
        private NavMeshAgent _navMeshAgent = null;
        private Collider _collider = null;

        public override void Move(GameObject target) {
            _target = target;
            if (_navMeshAgent.isOnNavMesh) {
                _navMeshAgent.isStopped = false;
                if (_target != null) {
                    _navMeshAgent.SetDestination(_target.transform.position);
                }
            }
        }

        public override void Idle() {
            if (_navMeshAgent.isOnNavMesh) {
                _navMeshAgent.isStopped = true;
            }
        }

        public override void Hit(GameObject target) {
            _target = target;
            if (_navMeshAgent.isOnNavMesh) {
                _navMeshAgent.isStopped = true;
            }
        }

        public override void Die() {
            _collider.enabled = false;
            if (_navMeshAgent.isOnNavMesh) {
                _navMeshAgent.isStopped = true;
            }
        }

        public override void Init() {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _collider = GetComponent<Collider>();
            _collider.enabled = true;

            if (_navMeshAgent.isOnNavMesh) {

                if (_speedData != null) {
                    _navMeshAgent.speed = _speedData.Speed;
                }
                if (_turnSpeedData != null) {
                    _navMeshAgent.angularSpeed = _turnSpeedData.Speed;
                }
            }
        }
    }
}

