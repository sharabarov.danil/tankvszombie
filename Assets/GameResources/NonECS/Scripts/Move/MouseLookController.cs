using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Look Controller
    /// </summary>
    public class MouseLookController : AbstractMoveController {

        private InputData _inputData;
        private CancellationTokenSource _cancelationTokenSource;

        private void Awake() {
            _inputData = new PlayerInputData();
        }

        private void OnEnable() {
            _inputData.Init();
            _inputData.OnStartFiring += StartFiring;
            _inputData.OnEndFiring += EndFiring;
        }

        private void OnDisable() {
            _inputData.Dispose();
            EndFiring();
            _inputData.OnStartFiring -= StartFiring;
            _inputData.OnEndFiring -= EndFiring;
        }

        private async void StartFiring() {
            _cancelationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = _cancelationTokenSource.Token;
            while (!cancellationToken.IsCancellationRequested) {
                Fire();
                await Task.Yield();
            }
        }

        private void Fire() {
            Vector3 mp = (Camera.main.ScreenToViewportPoint(_inputData.MousePosition) - new Vector3(0.5f, 0.5f, 0)) * 2;
            Move(mp);
        }

        protected override void Move(Vector2 direction) {
            Vector3 inputDirection = new Vector3(direction.x, 0, direction.y);

            if (inputDirection.magnitude > 0.1f) {

                var rotation = Quaternion.LookRotation(inputDirection);

                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _turnSpeedData.Speed);
            }
        }

        private void EndFiring() {
            if (_cancelationTokenSource != null) {
                _cancelationTokenSource.Cancel();
                _cancelationTokenSource = null;
            }
        }
    }
}
