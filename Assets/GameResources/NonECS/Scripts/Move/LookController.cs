using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Look Controller
    /// </summary>
    public class LookController : AbstractMoveController {

        private InputData _inputData;

        private void Awake() {
            _inputData = new PlayerInputData();
        }

        private void OnEnable() {
            _inputData.Init();
        }

        private void OnDisable() {
            _inputData.Dispose();
        }

        private void FixedUpdate() {
            Move(_inputData.Look);
        }

        protected override void Move(Vector2 direction) {
            Vector3 inputDirection = new Vector3(direction.x, 0, direction.y);

            if (inputDirection.magnitude > 0.1f) {

                var rotation = Quaternion.LookRotation(inputDirection);

                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _turnSpeedData.Speed);
            }
        }
    }
}
