﻿using UnityEngine;

namespace TankVsZombie.NoECS {
    /// <summary>
    /// Speed data
    /// </summary>

    [CreateAssetMenu(fileName = "SpeedData", menuName = "GameData/NoECS/Create SpeedData")]
    public class SpeedData : ScriptableObject {
        [Header("Speed:")]
        [SerializeField]
        private float _speed = 2.0f;

        public float Speed {
            get {
                return _speed;
            }
        }

    }
}
