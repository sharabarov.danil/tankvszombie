﻿using UnityEngine;

namespace TankVsZombie.NoECS {
    /// <summary>
    /// Room checker
    /// </summary>
    public class RoomChecker : MonoBehaviour {
        [SerializeField]
        private string _playerTag = "Player";
        [SerializeField]
        private Room room;

        private void OnCollisionEnter(Collision collision) {
            if (collision.gameObject.tag == _playerTag) {
                room.ContainPlayer = true;
            }
        }

        private void OnCollisionExit(Collision collision) {
            if (collision.gameObject.tag == _playerTag) {
                room.ContainPlayer = false;
            }
        }

    }
}
