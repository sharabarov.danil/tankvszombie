﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace TankVsZombie.NoECS {
    /// <summary>
    /// Room Creator
    /// </summary>

    public class RoomController : MonoBehaviour {
        [Header("Key for Pool:")]
        [SerializeField]
        private string _roomKey = "Room";
        [Header("Rooms count")]
        [Range(1, 50)]
        [SerializeField]
        private int _roomCount = 12;
        [Header("Central room")]
        [SerializeField]
        private int _center = 5;
        [Header("Room prefabs")]
        [SerializeField]
        private Room[] _roomPrefabs;

        private Room _startingRoom;

        private Room[,] spawnedRooms;

        public UnityEvent<Room[,]> OnCreaterAllRoom;

        private void Awake() {

            CreateFirstRoom();

            _center = Mathf.Clamp(_center, 1, _roomCount);
            spawnedRooms = new Room[_roomCount + 1, _roomCount + 1];
            spawnedRooms[_center, _center] = _startingRoom;

            for (int i = 0; i < _roomCount - 1; i++) {
                PlaceOneRoom();
            }

            OnCreaterAllRoom?.Invoke(spawnedRooms);
        }

        private void CreateFirstRoom() {
            _startingRoom = Pool.Instance.GetPoolElement(_roomPrefabs[UnityEngine.Random.Range(0, _roomPrefabs.Length)].gameObject, _roomKey).GetComponent<Room>();
            _startingRoom.transform.position = Vector3.zero;
            _startingRoom.ContainPlayer = true;
        }

        private void PlaceOneRoom() {
            HashSet<Vector2Int> vacantPlaces = new HashSet<Vector2Int>();
            for (int x = 0; x < spawnedRooms.GetLength(0); x++) {
                for (int y = 0; y < spawnedRooms.GetLength(1); y++) {
                    if (spawnedRooms[x, y] == null) continue;

                    int maxX = spawnedRooms.GetLength(0) - 1;
                    int maxY = spawnedRooms.GetLength(1) - 1;

                    if (x > 0 && spawnedRooms[x - 1, y] == null) vacantPlaces.Add(new Vector2Int(x - 1, y));
                    if (y > 0 && spawnedRooms[x, y - 1] == null) vacantPlaces.Add(new Vector2Int(x, y - 1));
                    if (x < maxX && spawnedRooms[x + 1, y] == null) vacantPlaces.Add(new Vector2Int(x + 1, y));
                    if (y < maxY && spawnedRooms[x, y + 1] == null) vacantPlaces.Add(new Vector2Int(x, y + 1));
                }
            }

            Room newRoom = Pool.Instance.GetPoolElement(_roomPrefabs[UnityEngine.Random.Range(0, _roomPrefabs.Length)].gameObject, _roomKey).GetComponent<Room>();//Instantiate(RoomPrefabs[UnityEngine.Random.Range(0, RoomPrefabs.Length)]);

            int limit = 500;
            while (limit-- > 0) {
                Vector2Int position = vacantPlaces.ElementAt(UnityEngine.Random.Range(0, vacantPlaces.Count));

                if (ConnectToSomething(newRoom, position)) {

                    newRoom.transform.position = new Vector3(position.x - _center, 0, position.y - _center) * newRoom.transform.localScale.x;
                    spawnedRooms[position.x, position.y] = newRoom;
                    return;
                }
            }

            Destroy(newRoom.gameObject);
        }

        private bool ConnectToSomething(Room room, Vector2Int p) {
            int maxX = spawnedRooms.GetLength(0) - 1;
            int maxY = spawnedRooms.GetLength(1) - 1;

            List<Vector2Int> neighbours = new List<Vector2Int>();

            if (room.CloseDoorU != null && p.y < maxY && spawnedRooms[p.x, p.y + 1]?.CloseDoorD != null) neighbours.Add(Vector2Int.up);
            if (room.CloseDoorD != null && p.y > 0 && spawnedRooms[p.x, p.y - 1]?.CloseDoorU != null) neighbours.Add(Vector2Int.down);
            if (room.CloseDoorR != null && p.x < maxX && spawnedRooms[p.x + 1, p.y]?.CloseDoorL != null) neighbours.Add(Vector2Int.right);
            if (room.CloseDoorL != null && p.x > 0 && spawnedRooms[p.x - 1, p.y]?.CloseDoorR != null) neighbours.Add(Vector2Int.left);

            if (neighbours.Count == 0) return false;

            Vector2Int selectedDirection = neighbours[UnityEngine.Random.Range(0, neighbours.Count)];
            Room selectedRoom = spawnedRooms[p.x + selectedDirection.x, p.y + selectedDirection.y];

            if (selectedDirection == Vector2Int.up) {
                room.ActiveUpDoor(false);
                selectedRoom.ActiveDownDoor(false);
            } else if (selectedDirection == Vector2Int.down) {
                room.ActiveDownDoor(false);
                selectedRoom.ActiveUpDoor(false);
            } else if (selectedDirection == Vector2Int.right) {
                room.ActiveRightDoor(false);
                selectedRoom.ActiveLeftDoor(false);
            } else if (selectedDirection == Vector2Int.left) {
                room.ActiveLeftDoor(false);
                selectedRoom.ActiveRightDoor(false);
            }

            return true;
        }
    }
}