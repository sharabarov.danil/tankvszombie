using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Enemy Generator
    /// </summary>
    public class EnemyGenerator : MonoBehaviour {

        [Header("Enemies:")]
        [SerializeField]
        private GameObject[] _enemies = new GameObject[0];

        [Header("Character's Key:")]
        [SerializeField]
        private string _enemyKey = "Monster";

        [Header("Character's count on start:")]
        [SerializeField]
        private Vector2Int _generateOnStartInRoom = new Vector2Int(0, 6);

        [Header("Radius for character's creation:")]
        [SerializeField]
        private float _radius = 1.0f;

        public static int CurrentEnemiesNumbers = 0;

        public void Generate(Room[,] rooms) {
            foreach (var item in rooms) {
                if (item != null) {
                    int rand = item.ContainPlayer ? 0 : UnityEngine.Random.Range(_generateOnStartInRoom.x, _generateOnStartInRoom.y);
                    for (int i = 0; i < rand; i++) {
                        CreateCharacter(RandomPlace(item));
                    }
                }
            }
        }

        /// <summary>
        /// Create character
        /// </summary>
        /// <param name="place"></param>

        private void CreateCharacter(Vector3 place) {
            int characterNumber = UnityEngine.Random.Range(0, _enemies.Length);
            GameObject randomCharacter = _enemies[characterNumber];
            GameObject character = Pool.Instance.GetPoolElement(randomCharacter, place, Quaternion.identity, _enemyKey + characterNumber);
            CurrentEnemiesNumbers++;
        }

        /// <summary>
        /// Random Place
        /// </summary>

        private Vector3 RandomPlace(Room room) {
            Vector3 point;

            if (RandomPoint(new Vector2(room.transform.position.x, room.transform.position.z), _radius, out point)) {
                Debug.DrawRay(point, Vector3.up, Color.blue, 1.0f);
                //return new Vector3(point.x, 0, point.y);
            }
            return room.transform.position;
        }

        private bool RandomPoint(Vector2 center, float range, out Vector3 result) {
            for (int i = 0; i < 10; i++) {
                Vector2 randomPoint = center + UnityEngine.Random.insideUnitCircle * range;
                result = randomPoint;
                return true;
                /*NavMeshHit hit;
                if (NavMesh.SamplePosition(randomPoint, out hit, Mathf.Infinity, NavMesh.AllAreas)) {
                    result = hit.position;
                    return true;
                }*/
            }
            result = Vector3.zero;
            return false;
        }
    }
}
