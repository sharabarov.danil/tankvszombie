﻿using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Damage Data
    /// </summary>
    [CreateAssetMenu(fileName = "DamageData", menuName = "GameData/NoECS/Create DamageData")]
    public class DamageData : ScriptableObject {
        [Header("Damage:")]
        [SerializeField]
        private float _damage = 10.0f;

        public float Damage {
            get {
                return _damage;
            }
        }
    }
}
