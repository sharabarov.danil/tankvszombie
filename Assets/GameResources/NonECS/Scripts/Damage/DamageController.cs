﻿using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Damage Controller
    /// </summary>

    public class DamageController : MonoBehaviour {

        [Header("Target Tag:")]
        [SerializeField]
        private string _targetTag = "Player";

        [Header("Damage Data:")]
        [SerializeField]
        private DamageData _damageData = null;

        [Header("Radius:")]
        [SerializeField]
        private float _radius = 1f;

        /// <summary>
        /// Collision Damage
        /// </summary>
        /// <param name="collision"></param>

        public void Damage() {
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, _radius);
            foreach (var collision in hitColliders) {
                if (_damageData != null && collision.gameObject.tag == _targetTag) {
                    if (collision.gameObject.GetComponent<HealthController>()) {
                        collision.gameObject.GetComponent<HealthController>().RemoveHealth(_damageData);
                    }
                }
            }
        }
    }
}

