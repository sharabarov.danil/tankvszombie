﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Object Pool
    /// </summary>

    public class Pool : MonoBehaviour {
        public static Pool Instance {
            get {
                return FindObjectOfType<Pool>();
            }
        }

        [SerializeField]
        protected List<ObjectPool> objectPools = new List<ObjectPool>();

        protected GameObject currentPoolElement = null;

        /// <summary>
        /// Get element from pool
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="poolID"></param>
        /// <param name="parent"></param>
        /// <returns></returns>

        public GameObject GetPoolElement(GameObject prefab, Vector3 position, Quaternion rotation, string poolID = default, Transform parent = default) {
            if (parent == null) {
                parent = transform;
            }
            if (prefab != null) {
                if (GetFreePoolElement(poolID) != null) {
                    currentPoolElement = GetFreePoolElement(poolID);
                    currentPoolElement.transform.SetParent(parent);
                    currentPoolElement.transform.position = position;
                    currentPoolElement.transform.rotation = rotation;
                } else {
                    currentPoolElement = (GameObject)GameObject.Instantiate(prefab, position, rotation);
                    currentPoolElement.transform.SetParent(parent);
                    if (GetObjectPool(poolID) != null) {
                        GetObjectPool(poolID).PoolElementObject.Add(currentPoolElement);
                    } else {
                        ObjectPool objectPool = new ObjectPool();
                        objectPool.poolID = poolID;
                        objectPool.PoolElementObject.Add(currentPoolElement);
                        objectPools.Add(objectPool);
                    }
                }
                currentPoolElement.SetActive(true);
                return currentPoolElement;
            }
            return null;
        }

        /// <summary>
        /// Get element from pool
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="poolID"></param>
        /// <param name="parent"></param>
        /// <returns></returns>

        public GameObject GetPoolElement(GameObject prefab, string poolID = default, Transform parent = default) {
            return GetPoolElement(prefab, transform.position, Quaternion.identity, poolID, parent);
        }

        /// <summary>
        /// Find free element
        /// </summary>
        /// <param name="poolID"></param>
        /// <returns></returns>
        protected GameObject GetFreePoolElement(string poolID) {
            if (GetObjectPool(poolID) == null) {
                return null;
            }

            return GetObjectPool(poolID).PoolElementObject.Find(x => !x.activeInHierarchy);
        }

        /// <summary>
        /// Remove element with delay
        /// </summary>
        /// <param name="poolElement"></param>
        /// <param name="speed"></param>
        public virtual void DestroyPoolElement(GameObject poolElement, float speed) {
            if (poolElement != null) {
                StartCoroutine(DestroyWithDelay(poolElement, speed));
            }
        }

        /// <summary>
        /// Remove pool element
        /// </summary>
        /// <param name="poolElement"></param>
        public virtual void DestroyPoolElement(GameObject poolElement) {
            if (poolElement != null) {
                poolElement.SetActive(false);
            }
        }

        protected virtual IEnumerator DestroyWithDelay(GameObject poolElement, float speed) {
            yield return new WaitForSecondsRealtime(speed);
            DestroyPoolElement(poolElement);
        }

        /// <summary>
        /// Remove all elemets
        /// </summary>
        public virtual void DestroyAllPoolElements() {
            for (int i = 0; i < objectPools.Count; i++) {
                for (int j = 0; j < objectPools[i].PoolElementObject.Count; j++) {
                    DestroyPoolElement(objectPools[i].PoolElementObject[j].gameObject);
                }
            }
        }

        /// <summary>
        /// Get pool element with poolID
        /// </summary>
        /// <param name="poolID"></param>
        /// <returns></returns>
        public ObjectPool GetObjectPool(string poolID) {
            return objectPools.Find(x => x.poolID == poolID);
        }

        [Serializable]
        public class ObjectPool {
            [Header("Key for pool:")]
            public string poolID = string.Empty;
            [Header("Pool's elements:")]
            public List<GameObject> PoolElementObject = new List<GameObject>();
        }
    }
}
