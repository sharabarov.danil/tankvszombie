﻿using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Health pool controller
    /// </summary>

    public class HealthPoolController : MonoBehaviour {
        public delegate void CreateHealthUIEventHandler(HealthController character, Color color);
        public static event CreateHealthUIEventHandler OnCreateHealthUI = delegate { };

        [SerializeField]
        private GameObject healthUIPrefab = null;

        [SerializeField]
        private Transform healthPoolPlace = null;

        [SerializeField]
        private string heaalthUIKey = "HealthUI";

        private void OnEnable() {
            OnCreateHealthUI += GenerateHealthUI;
        }

        private void OnDisable() {
            OnCreateHealthUI -= GenerateHealthUI;
        }

        /// <summary>
        /// Create HealthBar
        /// </summary>
        /// <param name="character"></param>
        /// <param name="color"></param>
        public static void CreateHealthUI(HealthController character, Color color) {
            if (OnCreateHealthUI != null) {
                OnCreateHealthUI(character, color);
            }
        }

        /// <summary>
        /// Generate health bar
        /// </summary>
        /// <param name="healthController"></param>
        /// <param name="color"></param>
        private void GenerateHealthUI(HealthController healthController, Color color) {
            GameObject characterhealthUI = Pool.Instance.GetPoolElement(healthUIPrefab, heaalthUIKey, healthPoolPlace);
            if (characterhealthUI != null) {
                if (characterhealthUI.GetComponent<HealthBar>() != null) {
                    characterhealthUI.GetComponent<HealthBar>().CurrentHealthController = healthController;
                    if (characterhealthUI.GetComponent<HealthBar>().HealthImage != null) {
                        characterhealthUI.GetComponent<HealthBar>().HealthImage.color = color;
                    }
                }
            }
        }
    }
}
