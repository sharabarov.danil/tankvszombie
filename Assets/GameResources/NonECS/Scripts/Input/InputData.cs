using System;
using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// InputData
    /// </summary>
    public abstract class InputData : MonoBehaviour {
        public abstract Vector2 Move {
            get;
        }

        public abstract Vector2 Look {
            get;
        }

        public abstract Vector3 MousePosition {
            get;
        }

        public Action OnStartFiring;

        public Action OnEndFiring;

        public Action OnNextWeapon;

        public Action OnPreviousWeapon;

        public abstract void Init();
        public abstract void Dispose();
    }

}

