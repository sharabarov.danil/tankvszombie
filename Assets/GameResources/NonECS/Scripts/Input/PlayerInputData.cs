using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Player Input Data
    /// </summary>
    public class PlayerInputData : InputData {

        private PlayerInputController _controls;

        public override Vector2 Move => _controls.Player.Movement.ReadValue<Vector2>();

        public override Vector2 Look => _controls.Player.Look.ReadValue<Vector2>();

        public override Vector3 MousePosition => _controls.Player.MousePosition.ReadValue<Vector2>();

        public PlayerInputData() {
            _controls = new PlayerInputController();
        }

        public override void Init() {
            _controls.Enable();
            _controls.Player.Shoot.started += _ => OnStartFiring?.Invoke();
            _controls.Player.Shoot.canceled += _ => OnEndFiring?.Invoke();
            _controls.Player.Next.performed += _ => OnNextWeapon?.Invoke();
            _controls.Player.Previous.performed += _ => OnPreviousWeapon?.Invoke();
        }

        public override void Dispose() {
            _controls.Disable();
            _controls.Player.Shoot.started -= _ => OnStartFiring?.Invoke();
            _controls.Player.Shoot.canceled -= _ => OnEndFiring?.Invoke();
            _controls.Player.Next.performed -= _ => OnNextWeapon?.Invoke();
            _controls.Player.Previous.performed -= _ => OnPreviousWeapon?.Invoke();
        }
    }
}
