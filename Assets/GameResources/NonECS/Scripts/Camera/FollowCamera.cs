﻿using UnityEngine;
using System.Collections;

namespace TankVsZombie.NoECS {
    /// <summary>
    /// Camera Movement
    /// </summary>
    public class FollowCamera : MonoBehaviour {
        [Header("Target:")]
        [SerializeField]
        public Transform TargetObject;
        [Header("Target distance:")]
        [SerializeField]
        private float followDistance = 5f;
        [Header("Height:")]
        [SerializeField]
        private float followHeight = 2f;
        [Header("Smoothness:")]
        [SerializeField]
        private bool smoothedFollow = false;
        [Header("Smooth speed:")]
        [SerializeField]
        private float smoothSpeed = 5f;

        private void FixedUpdate() {
            if (TargetObject != null) {
                Vector3 newPos;
                newPos = TargetObject.position - Vector3.up * followDistance;
                newPos.y = TargetObject.position.y + followHeight;

                if (!smoothedFollow) {
                    transform.position = newPos;
                } else {
                    transform.position = Vector3.Lerp(transform.position, newPos, Time.fixedDeltaTime * smoothSpeed);
                }

                Vector3 lookToward = TargetObject.position - transform.position;
                transform.forward = lookToward.normalized;
            }
        }

    }
}