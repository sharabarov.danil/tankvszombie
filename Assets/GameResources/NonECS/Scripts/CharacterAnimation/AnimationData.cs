﻿using UnityEngine;

namespace TankVsZombie.NoECS {
    /// <summary>
    /// Animation Data
    /// </summary>

    [CreateAssetMenu(fileName = "AnimationData", menuName = "GameData/NoECS/Create AnimationData")]
    public class AnimationData : ScriptableObject {
        [Header("Speed key:")]
        [SerializeField]
        private string _speedKey = "Speed";

        public string SpeedKey {
            get {
                return _speedKey;
            }
        }

        [Header("Idle key:")]
        [SerializeField]
        private string _idleKey = "Idle";

        public string IdleKey {
            get {
                return _idleKey;
            }
        }

        [Header("Die key:")]
        [SerializeField]
        private string _dieKey = "Die";

        public string DieKey {
            get {
                return _dieKey;
            }
        }

        [Header("Hit key:")]
        [SerializeField]
        private string _hitKey = "Hit";

        public string HitKey {
            get {
                return _hitKey;
            }
        }
    }
}
