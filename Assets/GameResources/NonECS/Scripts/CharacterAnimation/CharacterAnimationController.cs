﻿using UnityEngine;

namespace TankVsZombie.NoECS {
    /// <summary>
    /// Character animation
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class CharacterAnimationController : CharacterState {
        [Header("Speed key:")]
        [SerializeField]
        private AnimationData _animationData = null;

        [Header("Speed data:")]
        [SerializeField]
        private SpeedData _speedData = null;

        private Animator _animator = null;

        public override void Move(GameObject target) {
            _animator.SetBool(_animationData.HitKey, false);
            _animator.SetBool(_animationData.IdleKey, false);
        }

        public override void Idle() {
            _animator.SetBool(_animationData.HitKey, false);
            _animator.SetBool(_animationData.IdleKey, true);
        }

        public override void Hit(GameObject target) {
            _animator.SetBool(_animationData.HitKey, true);
        }

        public override void Die() {
            _animator.SetBool(_animationData.HitKey, false);
            _animator.SetBool(_animationData.IdleKey, true);
            _animator.SetBool(_animationData.DieKey, true);
        }

        public override void Init() {
            _animator = GetComponent<Animator>();
            _animator.SetFloat(_animationData.SpeedKey, _speedData.Speed);
        }
    }
}
