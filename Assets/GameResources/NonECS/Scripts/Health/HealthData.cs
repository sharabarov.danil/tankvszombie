﻿using UnityEngine;

namespace TankVsZombie.NoECS {
    /// <summary>
    /// Health Data
    /// </summary>
    [CreateAssetMenu(fileName = "HealthData", menuName = "GameData/NoECS/Create HealthData")]
    public class HealthData : ScriptableObject {
        [Header("Health:")]
        [SerializeField]
        private float _health = 100.0f;

        public float Health {
            get {
                return _health;
            }
        }

        [Header("Death time:")]
        [SerializeField]
        private int _deathTime = 5000;

        public int DeathTime {
            get {
                return _deathTime;
            }
        }
    }
}
