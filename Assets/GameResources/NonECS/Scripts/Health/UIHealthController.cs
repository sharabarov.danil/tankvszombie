using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// UIController for Health
    /// </summary>
    public class UIHealthController : MonoBehaviour {

        [Header("Color for HealthBar:")]
        [SerializeField]
        private Color _healthColor = Color.red;

        private HealthController _healthController;
        private bool _wasCreated = false;

        private void OnEnable() {
            _wasCreated = false;
            _healthController.OnRemoveHealth += RemoveHealth;
        }

        private void OnDisable() {
            _healthController.OnRemoveHealth -= RemoveHealth;
        }

        private void Awake() {
            _healthController = GetComponent<HealthController>();
        }

        private void RemoveHealth(float damage) {
            if (!_wasCreated) {
                _wasCreated = true;
                HealthPoolController.CreateHealthUI(_healthController, _healthColor);
            }
        }
    }
}
