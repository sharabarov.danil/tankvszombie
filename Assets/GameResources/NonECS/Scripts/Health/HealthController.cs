﻿using System;
using System.Threading.Tasks;
using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Health Controller
    /// </summary>
    public class HealthController : MonoBehaviour {
        [Header("Health Data:")]
        [SerializeField]
        private HealthData _healthData = null;

        public HealthData HealthInfo {
            get {
                return _healthData;
            }

            set {
                _healthData = value;
            }
        }

        public event Action<float> OnRemoveHealth = delegate { };
        public static event Action<string> OnDie = delegate { };

        private bool _isDead = false;

        public bool IsDead {
            get {
                return _isDead;
            }
        }

        private float _currentHealth = 0f;

        public float CurrentHealth {
            get {
                return _currentHealth;
            }
        }

        private void OnEnable() {
            if (_healthData != null) {
                _currentHealth = _healthData.Health;
            }
            _isDead = false;
        }

        /// <summary>
        /// Remove health
        /// </summary>
        /// <param name="damageInfo"></param>
        public void RemoveHealth(DamageData damageInfo) {


            if (damageInfo != null && _healthData != null) {
                float damage = damageInfo.Damage;
                _currentHealth -= damage;
                OnRemoveHealth?.Invoke(damage);
            }

            if (_currentHealth <= 0f && !IsDead) {
                _currentHealth = 0f;
                Die();
            }
        }

        /// <summary>
        /// Die
        /// </summary>

        private void Die() {
            _isDead = true;
            OnDie?.Invoke(gameObject.tag);
            DisableEnemy();
        }

        private async void DisableEnemy() {
            await Task.Delay(_healthData.DeathTime);
            Pool.Instance.DestroyPoolElement(this.gameObject);
        }
    }
}
