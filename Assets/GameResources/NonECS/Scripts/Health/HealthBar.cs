﻿using UnityEngine;
using UnityEngine.UI;

namespace TankVsZombie.NoECS {
    /// <summary>
    /// Health Bar
    /// </summary>

    public class HealthBar : MonoBehaviour {
        [Header("Health filler:")]
        [SerializeField]
        private Image healthImage = null;

        public Image HealthImage {
            get {
                return healthImage;
            }
        }

        private HealthController healthController = null;

        public HealthController CurrentHealthController {
            set {
                healthController = value;
            }
        }

        private void FixedUpdate() {
            HealthView();
        }

        /// <summary>
        /// Health View
        /// </summary>

        private void HealthView() {
            if (healthController != null) {
                if (healthImage != null) {
                    healthImage.fillAmount = healthController.CurrentHealth / healthController.HealthInfo.Health;
                    if (healthController.IsDead) {
                        this.gameObject.SetActive(false);
                    }
                    UpdatePoint(healthController.gameObject.transform.position);
                }
            }
        }

        /// <summary>
        /// Health View Position
        /// </summary>
        /// <param name="globalPosition"></param>

        private void UpdatePoint(Vector3 globalPosition) {
            this.gameObject.GetComponent<RectTransform>().position = RectTransformUtility.WorldToScreenPoint(Camera.main, globalPosition + Vector3.up * 3);
        }
    }

}
