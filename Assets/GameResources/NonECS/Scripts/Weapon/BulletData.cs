﻿using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Bullet data
    /// </summary>
    [CreateAssetMenu(fileName = "BullerData", menuName = "GameData/NoECS/Create BulletData")]
    public class BulletData : ScriptableObject {
        [Header("Speed:")]
        [SerializeField]
        private float _speed = 30.0f;

        public float Speed {
            get {
                return _speed;
            }
        }

        [Header("Frequency:")]
        [SerializeField]
        private int _frequency = 300;

        public int Frequency {
            get {
                return _frequency;
            }
        }

        [Header("Life time:")]
        [SerializeField]
        private float _lifeTime = 1.0f;

        public float LifeTime {
            get {
                return _lifeTime;
            }
        }
    }
}
