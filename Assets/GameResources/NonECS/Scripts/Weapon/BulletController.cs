﻿using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Bullet controller
    /// </summary>
    public class BulletController : MonoBehaviour {
        [Header("Player Tag:")]
        [SerializeField]
        private string _playerTag;

        [Header("Bullet Data:")]
        [SerializeField]
        private BulletData _bulletData = null;

        [Header("Damage Data:")]
        [SerializeField]
        private DamageData _damageData = null;

        private float currentTime = 0f;

        /// <summary>
        /// Trigger for bullets
        /// </summary>
        /// <param name="collider"></param>

        private void OnTriggerEnter(Collider collider) {
            if (collider.gameObject.tag != _playerTag) {
                if (_damageData != null && collider.gameObject.GetComponent<HealthController>()) {
                    collider.gameObject.GetComponent<HealthController>().RemoveHealth(_damageData);
                }

                DestroyBullet();
            }
        }

        private void OnEnable() {
            currentTime = 0;
        }

        protected void Update() {
            Move();
        }

        /// <summary>
        /// Bullet Movement
        /// </summary>

        private void Move() {
            if (_bulletData != null) {
                transform.Translate(Vector3.forward * Time.deltaTime * _bulletData.Speed);
                if (currentTime >= _bulletData.LifeTime) {
                    DestroyBullet();
                } else {
                    currentTime += Time.deltaTime;
                }
            }
        }

        /// <summary>
        /// Destroy bullet
        /// </summary>

        private void DestroyBullet() {
            Pool.Instance.DestroyPoolElement(this.gameObject);
        }

    }
}
