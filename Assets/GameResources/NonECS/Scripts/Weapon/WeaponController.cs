﻿using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Weapon Controller
    /// </summary>

    public class WeaponController : MonoBehaviour {

        [Header("Place")]
        [SerializeField]
        private Transform _weaponTransform;

        [Header("Weapons:")]
        [SerializeField]
        private Weapon[] _weapons = new Weapon[0];

        private InputData _inputData;
        private int _currentWeaponNumber = 0;
        private Weapon _currentWeapon;

        private void Awake() {
            _inputData = new PlayerInputData();
            _currentWeaponNumber = 0;
            ActivateWeapon();
        }


        private void OnEnable() {
            _inputData.Init();
            _inputData.OnNextWeapon += NextWeapon;
            _inputData.OnPreviousWeapon += PreviousWeapon;
        }

        private void OnDisable() {
            _inputData.Dispose();
            _inputData.OnNextWeapon -= NextWeapon;
            _inputData.OnPreviousWeapon -= PreviousWeapon;
        }

        /// <summary>
        /// Next weapon
        /// </summary>

        private void NextWeapon() {
            _currentWeaponNumber++;
            if (_currentWeaponNumber >= _weapons.Length) {
                _currentWeaponNumber = 0;
            }
            ActivateWeapon();
        }

        /// <summary>
        /// Previous weapon
        /// </summary>

        private void PreviousWeapon() {
            _currentWeaponNumber--;
            if (_currentWeaponNumber < 0) {
                _currentWeaponNumber = _weapons.Length - 1;
            }
            ActivateWeapon();
        }

        /// <summary>
        /// Activate weapon
        /// </summary>

        private void ActivateWeapon() {
            if (_currentWeapon != null) {
                Pool.Instance.DestroyPoolElement(_currentWeapon.gameObject);
            }
            for (int i = 0; i < _weapons.Length; i++) {
                if (_currentWeaponNumber == i) {
                    _currentWeapon = CreateWeapon(_weapons[i]);
                }
            }
        }

        private Weapon CreateWeapon(Weapon weapon) {
            GameObject currentWeapon = Pool.Instance.GetPoolElement(weapon.gameObject, weapon.name, _weaponTransform);
            if (currentWeapon != null) {
                if (_weaponTransform != null) {
                    currentWeapon.transform.localPosition = weapon.transform.localPosition;
                    currentWeapon.transform.localRotation = weapon.transform.localRotation;
                }
            }
            return currentWeapon.GetComponent<Weapon>();
        }
    }
}
