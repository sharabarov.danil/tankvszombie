﻿using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Weapon Controller
    /// </summary>

    public class Weapon : MonoBehaviour {
        [Header("Bullet Prefab:")]
        [SerializeField]
        private GameObject _bulletPrefab = null;

        [Header("Bullet Start Place:")]
        [SerializeField]
        private Transform _bulletStartPlace = null;

        [Header("Bullet Data:")]
        [SerializeField]
        private BulletData _bulletData = null;

        [Header("Key for bullets in Pool:")]
        [SerializeField]
        private string _bulletKey = "Bullet";

        private InputData _inputData;
        private CancellationTokenSource _cancelationTokenSource;

        private void Awake() {
            _inputData = new PlayerInputData();
        }

        private void OnEnable() {
            _inputData.Init();
            _inputData.OnStartFiring += Fire;
            _inputData.OnEndFiring += Cancel;
        }

        private void OnDisable() {
            Cancel();
            _inputData.Dispose();
            _inputData.OnStartFiring -= Fire;
            _inputData.OnEndFiring -= Cancel;
        }

        /// <summary>
        /// Activate weapon
        /// </summary>

        public void Activate() {
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Deactivate weapon
        /// </summary>

        public void Deactivate() {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Fire
        /// </summary>

        private async void Fire() {
            _cancelationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = _cancelationTokenSource.Token;
            while (!cancellationToken.IsCancellationRequested) {
                GameObject bullet = Pool.Instance.GetPoolElement(_bulletPrefab, _bulletKey);
                if (bullet != null) {
                    if (_bulletStartPlace != null) {
                        bullet.transform.position = _bulletStartPlace.position;
                        bullet.transform.rotation = _bulletStartPlace.rotation;
                    } else {
                        bullet.transform.position = transform.position;
                        bullet.transform.rotation = Quaternion.identity;
                    }
                }
                await Task.Delay(_bulletData.Frequency);
            }
        }

        private void Cancel() {
            if (_cancelationTokenSource != null) {
                _cancelationTokenSource.Cancel();
                _cancelationTokenSource = null;
            }
        }
    }
}
