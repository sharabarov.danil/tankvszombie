﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace TankVsZombie.NoECS {

    /// <summary>
    /// Game Panel
    /// </summary>
    public class GamePanel : MonoBehaviour {

        [Header("Player Tag")]
        [SerializeField]
        private string _playerTag = "Player";

        [Header("Current score:")]
        [SerializeField]
        private Text scoreText = null;

        [Header("Defeat text:")]
        [SerializeField]
        private GameObject defeatText = null;

        [Header("Victory text:")]
        [SerializeField]
        private GameObject victoryText = null;

        private const string SCENE_NAME = "NoEcsScene";

        private void OnEnable() {
            ScoreView();
            HealthController.OnDie += Die;
        }

        private void OnDisable() {
            HealthController.OnDie -= Die;
        }

        /// <summary>
        /// Score view
        /// </summary>
        private void ScoreView() {
            if (scoreText != null) {
                scoreText.text = "Enemies Count: " + EnemyGenerator.CurrentEnemiesNumbers;
            }
        }

        /// <summary>
        /// Adding count in death
        /// </summary>
        private void KillEnemy() {
            ScoreView();
            EnemyGenerator.CurrentEnemiesNumbers--;
            if (EnemyGenerator.CurrentEnemiesNumbers <= 0) {
                EnemyGenerator.CurrentEnemiesNumbers = 0;
                Victory();
            }
        }

        private void Die(string tag) {
            if (tag != _playerTag) {
                KillEnemy();
            } else {
                Defeat();
            }
        }

        /// <summary>
        /// Restart game
        /// </summary>
        public void RestartGame() {
            SceneManager.LoadScene(SCENE_NAME);
        }

        private void Defeat() {
            if (defeatText != null) {
                defeatText.SetActive(true);
            }
        }

        private void Victory() {
            EnemyGenerator.CurrentEnemiesNumbers = 0;
            if (victoryText != null) {
                victoryText.SetActive(true);
            }
        }
    }
}
