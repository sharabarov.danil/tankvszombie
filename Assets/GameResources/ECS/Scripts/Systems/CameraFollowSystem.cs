using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Camera follow
    /// </summary>
    public class CameraFollowSystem : IEcsRunSystem, IEcsInitSystem {

        private EcsWorld _world;
        private CameraData _cameraData;
        private Pool _pool;

        private readonly EcsFilter<PlayerTagComponent> _tagFilter;
        private readonly EcsFilter<ModelComponent, CameraComponent, CameraMoveComponent> _filters = null;

        public void Init() {
            Transform characterPlace = null;

            foreach (var i in _tagFilter) {
                ref var entity = ref _tagFilter.GetEntity(i);
                ref var target = ref entity.Get<ModelComponent>();

                characterPlace = target.ModelTransform;
            }

            GameObject followCamera = _pool.Spawn(_cameraData.FollowCamera.gameObject);

            var camera = _world.NewEntity();
            ref var modelComponent = ref camera.Get<ModelComponent>();
            ref var cameraComponent = ref camera.Get<CameraComponent>();
            ref var cameraSettingsComponent = ref camera.Get<CameraMoveComponent>();

            modelComponent.ModelTransform = characterPlace;

            cameraComponent.FollowCamera = followCamera.GetComponent<Camera>();
            cameraSettingsComponent.FollowDistance = _cameraData.FollowDistance;
            cameraSettingsComponent.FollowHeight = _cameraData.FollowHeight;
            cameraSettingsComponent.SmoothedFollow = _cameraData.SmoothedFollow;
            cameraSettingsComponent.SmoothSpeed = _cameraData.SmoothSpeed;
        }

        public void Run() {
            foreach (var i in _filters) {
                ref var modelComponent = ref _filters.Get1(i);
                ref var cameraComponent = ref _filters.Get2(i);
                ref var cameraSettingsComponent = ref _filters.Get3(i);

                ref var target = ref modelComponent.ModelTransform;
                ref var followCamera = ref cameraComponent.FollowCamera;
                ref var followDistance = ref cameraSettingsComponent.FollowDistance;
                ref var followHeight = ref cameraSettingsComponent.FollowHeight;
                ref var smoothedFollow = ref cameraSettingsComponent.SmoothedFollow;
                ref var smoothSpeed = ref cameraSettingsComponent.SmoothSpeed;

                if (target != null) {
                    Vector3 newPos;
                    newPos = target.position - Vector3.up * followDistance;
                    newPos.y = target.position.y + followHeight;

                    if (!smoothedFollow) {
                        followCamera.transform.position = newPos;
                    } else {
                        followCamera.transform.position = Vector3.Lerp(followCamera.transform.position, newPos, Time.fixedDeltaTime * smoothSpeed);
                    }

                    Vector3 lookToward = target.position - followCamera.transform.position;
                    followCamera.transform.forward = lookToward.normalized;
                }

            }

        }
    }
}
