using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// HealthBar Position System
    /// </summary>
    public class HealthBarPositionSystem : IEcsRunSystem {

        private readonly EcsFilter<CameraComponent> _cameraFilter;

        private readonly EcsFilter<HealthBarComponent> _filters;

        public void Run() {
            foreach (var i in _filters) {
                ref var healthBarComponent = ref _filters.Get1(i);
                if (healthBarComponent.HealthTarget.IsAlive()) {
                    ref var modelComponent = ref healthBarComponent.HealthTarget.Get<ModelComponent>();

                    foreach (var j in _cameraFilter) {
                        ref var cameraComponent = ref _cameraFilter.Get1(j);

                        healthBarComponent.HealthBar.GetComponent<RectTransform>().position = RectTransformUtility.WorldToScreenPoint(cameraComponent.FollowCamera, modelComponent.ModelTransform.position + Vector3.up * 3);
                    }
                }
            }
        }
    }
}
