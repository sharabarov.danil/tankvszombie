using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Hit System
    /// </summary>
    public class HitSystem : IEcsRunSystem {

        private Pool _pool;
        private readonly EcsFilter<AttackComponent, HitComponent> _hitFilter;

        public void Run() {
            foreach (var i in _hitFilter) {
                ref var entity = ref _hitFilter.GetEntity(i);
                ref var attackComponent = ref _hitFilter.Get1(i);
                ref var hitComponent = ref _hitFilter.Get2(i);

                if (hitComponent.OtherTriggerEntity.IsAlive() && hitComponent.OtherTriggerEntity.Has<HealthComponent>()) {

                    ref var healthComponent = ref hitComponent.OtherTriggerEntity.Get<HealthComponent>();
                    if (attackComponent.MyTriggerEntity.IsAlive()) {
                        ref var damageComponent = ref attackComponent.MyTriggerEntity.Get<DamageComponent>();
                        healthComponent.CurrentHealth -= damageComponent.Damage;
                    }
                }

                if (attackComponent.MyTriggerEntity.IsAlive()) {
                    if (attackComponent.MyTriggerEntity.Has<ModelComponent>()) {
                        ref var modelComponent = ref attackComponent.MyTriggerEntity.Get<ModelComponent>();
                        _pool.Despawn(modelComponent.ModelTransform.gameObject);
                    }

                    attackComponent.MyTriggerEntity.Destroy();
                }

                entity.Destroy();
            }
        }
    }

}
