﻿using Leopotam.Ecs;

namespace TankVsZombie.ECS {

    /// <summary>
    /// End GameInit System
    /// </summary>
    public class EndGameInitSystem : IEcsInitSystem {

        private EcsWorld _world;
        private EndGameView _endGameView;

        public void Init() {
            var entity = _world.NewEntity();
            ref var endGameComponent = ref entity.Get<EndGameComponent>();
            endGameComponent.View = _endGameView;
        }
    }
}