using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// LifeTime System
    /// </summary>
    public class LifeTimeSystem : IEcsRunSystem {

        private Pool _pool;

        private readonly EcsFilter<BulletTagComponent, ModelComponent, LifeTimeComponent> _filter = null;

        public void Run() {
            foreach (var i in _filter) {
                ref var bullet = ref _filter.GetEntity(i);
                ref var modelComponent = ref _filter.Get2(i);
                ref var lifeTimeComponent = ref _filter.Get3(i);

                if (lifeTimeComponent.LifeTime < 0) {
                    _pool.Despawn(modelComponent.ModelTransform.gameObject);
                    bullet.Destroy();
                } else {
                    lifeTimeComponent.LifeTime -= Time.deltaTime;
                }

            }
        }
    }

}
