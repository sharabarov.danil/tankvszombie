using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Weapons Shoot System
    /// </summary>
    public class WeaponsShootSystem : IEcsInitSystem, IEcsDestroySystem {

        private EcsWorld _world;
        private Pool _pool;

        private readonly EcsFilter<PlayerTagComponent> _playerTagFilter;
        private readonly EcsFilter<WeaponTagComponent> _weaponTagFilter;

        private InputAction _shootAction;
        private CancellationTokenSource _cancelationTokenSource;
        private Transform _bulletPlace;
        private int _frequency;
        private BulletData _bulletData;

        public void Init() {

            foreach (var i in _playerTagFilter) {
                ref var entity = ref _playerTagFilter.GetEntity(i);
                ref var inputComponent = ref entity.Get<InputComponent>();

                _shootAction = inputComponent.Actions.Shoot;
            }
            _shootAction.started += _ => Fire();
            _shootAction.canceled += _ => Cancel();
        }

        private void CheckWeapon() {
            foreach (var i in _weaponTagFilter) {
                ref var entity = ref _weaponTagFilter.GetEntity(i);
                ref var weaponModelComponent = ref entity.Get<ModelComponent>();
                ref var weaponComponent = ref entity.Get<WeaponComponent>();

                _bulletPlace = weaponModelComponent.ModelTransform.GetComponent<ElementPlace>().Place;
                _bulletData = weaponComponent.BulletData;
                _frequency = weaponComponent.Frequency;
            }
        }

        private void CreateBulletEntity(BulletData bulletData) {

            GameObject model = _pool.Spawn(_bulletData.Bullet, _bulletPlace.position, _bulletPlace.rotation);

            var bullet = _world.NewEntity();

            ref var bulletTagComponent = ref bullet.Get<BulletTagComponent>();

            ref var modelComponent = ref bullet.Get<ModelComponent>();
            modelComponent.ModelTransform = model.transform;

            ref var movableComponent = ref bullet.Get<SpeedComponent>();
            movableComponent.Speed = bulletData.Speed;

            ref var lifeTimeComponent = ref bullet.Get<LifeTimeComponent>();
            lifeTimeComponent.LifeTime = bulletData.LifeTime;

            ref var damageComponent = ref bullet.Get<DamageComponent>();
            damageComponent.Damage = bulletData.Damage;

            ref var triggerLayerrComponent = ref bullet.Get<TriggerLayerComponent>();
            triggerLayerrComponent.IgnoreLayer = bulletData.IgnoreLayer;

            ref var triggerCheckerComponent = ref bullet.Get<TriggerCheckerComponent>();
            triggerCheckerComponent.TriggerChecker = model.GetComponent<ProjectileAttackDealer>();
            triggerCheckerComponent.TriggerChecker.Init(_world, _pool, bullet);
        }

        private async void Fire() {
            _cancelationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = _cancelationTokenSource.Token;

            CheckWeapon();

            while (!cancellationToken.IsCancellationRequested && _playerTagFilter.GetEntitiesCount() > 0) {
                CreateBulletEntity(_bulletData);
                await Task.Delay(_frequency);
            }
        }

        private void Cancel() {
            if (_cancelationTokenSource != null) {
                _cancelationTokenSource.Cancel();
                _cancelationTokenSource = null;
            }
        }

        public void Destroy() {
            _shootAction.started -= _ => Fire();
            _shootAction.canceled -= _ => Cancel();
            Cancel();
        }
    }
}
