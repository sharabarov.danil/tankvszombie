using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Death Anim System
    /// </summary>
    public class DeathAnimSystem : IEcsRunSystem {

        private readonly EcsFilter<AnimatorComponent, NavMeshComponent, DeathEvent> _filter;

        public void Run() {
            foreach (var i in _filter) {
                ref var animatorComponent = ref _filter.Get1(i);
                ref var navMeshComponent = ref _filter.Get2(i);
                navMeshComponent.NavMeshAgent.isStopped = true;
                navMeshComponent.NavMeshAgent.speed = 0;
                navMeshComponent.NavMeshAgent.angularSpeed = 0;

                animatorComponent.Animator.SetTrigger("Die");
            }
        }
    }
}
