using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Death System
    /// </summary>
    public class DeathSystem : IEcsRunSystem {

        private readonly EcsFilter<HealthComponent>.Exclude<DeathEvent> _filter;

        public void Run() {
            foreach (var i in _filter) {
                ref var entity = ref _filter.GetEntity(i);
                ref var healthComponent = ref _filter.Get1(i);
                if (healthComponent.CurrentHealth <= 0) {
                    healthComponent.CurrentHealth = 0;
                    entity.Get<DeathEvent>();
                }

            }
        }
    }
}
