using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Trigger System
    /// </summary>
    public class TriggerSystem : IEcsRunSystem {

        private readonly EcsFilter<BulletTagComponent> _filters;

        public void Run() {
            foreach (var i in _filters) {
                ref var bulletTagComponent = ref _filters.Get1(i);


            }
        }
    }
}
