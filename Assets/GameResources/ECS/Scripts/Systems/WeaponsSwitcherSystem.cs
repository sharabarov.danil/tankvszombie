using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Weapons Switcher System
    /// </summary>
    public class WeaponsSwitcherSystem : IEcsInitSystem, IEcsDestroySystem {

        private EcsWorld _world;
        private PlayerData _playerData;
        private Pool _pool;

        private int _currentWeaponNumber = 0;
        private GameObject _currentWeapon = null;
        private Transform _weaponPlace = null;

        private readonly EcsFilter<PlayerTagComponent> _playerTagFilter;


        private EcsEntity _weapon;
        private InputAction _nextWeapon;
        private InputAction _previousWeapon;

        public void Init() {
            _weapon = _world.NewEntity();

            foreach (var i in _playerTagFilter) {
                ref var entity = ref _playerTagFilter.GetEntity(i);
                ref var target = ref entity.Get<WeaponModelComponent>();
                ref var inputActions = ref entity.Get<InputComponent>();

                _weaponPlace = target.ModelTransform;
                _previousWeapon = inputActions.Actions.Previous;
                _nextWeapon = inputActions.Actions.Next;
            }

            if (_playerData.Weapons.Length > 0) {
                CreateWeapon(_playerData.Weapons[0]);
            }

            _nextWeapon.performed += _ => NextWeapon();
            _previousWeapon.performed += _ => PreviousWeapon();
        }

        /// <summary>
        /// Next weapon
        /// </summary>

        private void NextWeapon() {
            _currentWeaponNumber++;
            if (_currentWeaponNumber >= _playerData.Weapons.Length) {
                _currentWeaponNumber = 0;
            }
            ActivateWeapon();
        }

        /// <summary>
        /// Previous weapon
        /// </summary>

        private void PreviousWeapon() {
            _currentWeaponNumber--;
            if (_currentWeaponNumber < 0) {
                _currentWeaponNumber = _playerData.Weapons.Length - 1;
            }
            ActivateWeapon();
        }

        /// <summary>
        /// Activate weapon
        /// </summary>

        private void ActivateWeapon() {
            CreateWeapon(_playerData.Weapons[_currentWeaponNumber]);
        }

        private void CreateWeapon(WeaponData weaponData) {
            if (_currentWeapon != null) {
                _pool.Despawn(_currentWeapon);
            }

            ref var weaponComponent = ref _weapon.Get<WeaponComponent>();
            ref var modelComponent = ref _weapon.Get<ModelComponent>();
            ref var weaponTagComponent = ref _weapon.Get<WeaponTagComponent>();
            weaponComponent.Frequency = weaponData.Frequency;
            weaponComponent.BulletData = weaponData.BulletData;

            _currentWeapon = _pool.Spawn(weaponData.WeaponPrefab, _weaponPlace);

            _currentWeapon.transform.localPosition = weaponData.WeaponPrefab.transform.localPosition;
            _currentWeapon.transform.localRotation = weaponData.WeaponPrefab.transform.localRotation;

            modelComponent.ModelTransform = _currentWeapon.transform;
        }

        public void Destroy() {
            _nextWeapon.performed -= _ => NextWeapon();
            _previousWeapon.performed -= _ => PreviousWeapon();
        }
    }
}
