using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Translate System
    /// </summary>
    sealed class TranslateSystem : IEcsRunSystem {

        private readonly EcsFilter<BulletTagComponent, ModelComponent, SpeedComponent> _filter = null;

        private Vector3 direction;

        public void Run() {

            foreach (var i in _filter) {
                ref var modelComponent = ref _filter.Get2(i);
                ref var speedComponent = ref _filter.Get3(i);

                ref var transform = ref modelComponent.ModelTransform;
                ref var speed = ref speedComponent.Speed;

                direction = Vector3.forward;

                if (direction.magnitude > 0.1f) {

                    transform.Translate(direction * speed * Time.deltaTime);

                }

            }
        }
    }
}
