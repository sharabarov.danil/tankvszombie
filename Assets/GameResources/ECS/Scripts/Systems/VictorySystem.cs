using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Victory System
    /// </summary>
    public class VictorySystem : IEcsRunSystem {

        private readonly EcsFilter<EndGameComponent> _endGameFilter;
        private readonly EcsFilter<EnemyTagComponent, HealthComponent> _filters;
        public void Run() {
            foreach (var i in _endGameFilter) {
                ref var entity = ref _endGameFilter.GetEntity(i);
                ref var endGameComponent = ref _endGameFilter.Get1(i);
                if (_filters.GetEntitiesCount() == 0) {
                    endGameComponent.View.ShowStatus(true);
                    entity.Destroy();
                }
            }
        }

    }
}