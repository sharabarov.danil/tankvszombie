﻿using Leopotam.Ecs;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Enemies Count System
    /// </summary>
    public class EnemiesCountUISystem : IEcsInitSystem {

        private EcsWorld _world;
        private EnemiesCountView _enemiesCountView;

        public void Init() {
            var entity = _world.NewEntity();
            ref var enemiesCountComponent = ref entity.Get<EnemiesCountComponent>();
            enemiesCountComponent.View = _enemiesCountView;
        }

    }
}
