using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Game Init System
    /// </summary>
    sealed class PlayerGeneratorSystem : IEcsInitSystem, IEcsDestroySystem {

        private EcsWorld _world;
        private PlayerData _playerData;
        private HealthBarSourceView _healthBarSourceView;
        private Pool _pool;
        private PlayerInputController _playerInputController;

        public void Destroy() {
            DestroyInput();
        }

        public void Init() {
            InitInput();
            InitPlayer();
        }

        private void InitInput() {
            _playerInputController = new PlayerInputController();
            _playerInputController.Enable();
        }

        private void DestroyInput() {
            _playerInputController.Disable();
        }

        private void InitPlayer() {

            var spawnedPlayer = _pool.Spawn(_playerData.PlayerPrefab);

            var player = _world.NewEntity();

            ref var playerTagComponent = ref player.Get<PlayerTagComponent>();

            ref var modelComponent = ref player.Get<ModelComponent>();
            modelComponent.ModelTransform = spawnedPlayer.transform;

            ref var movableComponent = ref player.Get<CharacterControllerComponent>();
            movableComponent.CharacterController = spawnedPlayer.GetComponent<CharacterController>();

            ref var speedComponent = ref player.Get<SpeedComponent>();
            speedComponent.Speed = _playerData.Speed;

            ref var turningComponent = ref player.Get<TurningComponent>();
            turningComponent.TurnSpeed = _playerData.TurnSpeed;

            ref var inputComponent = ref player.Get<InputComponent>();
            inputComponent.Actions = _playerInputController.Player;

            ref var directionComponent = ref player.Get<DirectionComponent>();
            directionComponent.Direction = inputComponent.Actions.Movement;

            ref var weaponModelComponent = ref player.Get<WeaponModelComponent>();
            weaponModelComponent.ModelTransform = spawnedPlayer.GetComponent<ElementPlace>().Place;

            ref var healthComponent = ref player.Get<HealthComponent>();
            healthComponent.CurrentHealth = _playerData.Health;
            healthComponent.MaxHealth = _playerData.Health;

            ref var entityRefComponent = ref player.Get<EntityRefComponent>();
            entityRefComponent.EntityRef = spawnedPlayer.GetComponent<EntityView>();
            entityRefComponent.EntityRef.Entity = player;

            var spawnedHealthBar = _pool.Spawn(_playerData.HealthBar.gameObject, _healthBarSourceView.SourceView);

            var healthBarEntity = _world.NewEntity();

            ref var healthBarComponent = ref healthBarEntity.Get<HealthBarComponent>();
            healthBarComponent.HealthBar = spawnedHealthBar.GetComponent<Image>();
            healthBarComponent.HealthTarget = player;
        }


    }
}