using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Direction System
    /// </summary>
    public class DirectionSystem : IEcsRunSystem {

        private readonly EcsWorld _world = null;
        private readonly EcsFilter<InputComponent, DirectionComponent> _filters = null;

        public void Run() {
            foreach (var i in _filters) {
                ref var inputComponent = ref _filters.Get1(i);
                ref var directionComponent = ref _filters.Get2(i);
                directionComponent.Direction = inputComponent.Actions.Movement;
            }
        }
    }
}
