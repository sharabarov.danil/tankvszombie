﻿using Leopotam.Ecs;

namespace TankVsZombie.ECS {
    /// <summary>
    /// HealthBar System
    /// </summary>
    public class HealthBarSystem : IEcsRunSystem {

        private Pool _pool;
        private readonly EcsFilter<HealthBarComponent> _filters;

        public void Run() {
            foreach (var i in _filters) {
                ref var entity = ref _filters.GetEntity(i);
                ref var healthBarComponent = ref _filters.Get1(i);
                if (healthBarComponent.HealthTarget.IsAlive()) {
                    ref var healthComponent = ref healthBarComponent.HealthTarget.Get<HealthComponent>();

                    healthBarComponent.HealthBar.fillAmount = (float)healthComponent.CurrentHealth / (float)healthComponent.MaxHealth;
                    if (healthComponent.CurrentHealth < 0) {
                        healthComponent.CurrentHealth = 0;
                        _pool.Despawn(healthBarComponent.HealthBar.gameObject);
                        entity.Destroy();
                    }
                } else {
                    _pool.Despawn(healthBarComponent.HealthBar.gameObject);
                }
            }
        }
    }
}
