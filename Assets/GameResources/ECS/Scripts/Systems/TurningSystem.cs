using Leopotam.Ecs;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Look System
    /// </summary>
    sealed class TurningSystem : IEcsRunSystem {

        private readonly EcsWorld _world = null;
        private readonly EcsFilter<ModelComponent, TurningComponent, DirectionComponent> turningFilter = null;
        private Vector3 direction;
        public void Run() {
            foreach (var i in turningFilter) {
                ref var modelComponent = ref turningFilter.Get1(i);
                ref var turningComponent = ref turningFilter.Get2(i);
                ref var directionComponent = ref turningFilter.Get3(i);

                ref var directionValue = ref directionComponent.Direction;
                ref var transform = ref modelComponent.ModelTransform;
                ref var turnSpeed = ref turningComponent.TurnSpeed;

                direction = new Vector3(directionValue.ReadValue<Vector2>().x, 0, directionValue.ReadValue<Vector2>().y);

                if (direction.magnitude > 0.1f) {

                    var rotation = Quaternion.LookRotation(direction);

                    transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * turnSpeed);
                }

            }
        }
    }
}
