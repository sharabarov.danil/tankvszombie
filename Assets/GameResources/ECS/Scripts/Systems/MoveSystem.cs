using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Move System
    /// </summary>
    sealed class MoveSystem : IEcsRunSystem {

        private readonly EcsFilter<PlayerTagComponent, CharacterControllerComponent, DirectionComponent, SpeedComponent> _filter = null;

        private Vector3 direction;

        public void Run() {

            foreach (var i in _filter) {
                ref var movableComponent = ref _filter.Get2(i);
                ref var directionComponent = ref _filter.Get3(i);
                ref var speedComponent = ref _filter.Get4(i);

                ref var movement = ref directionComponent.Direction;

                ref var characterController = ref movableComponent.CharacterController;
                ref var speed = ref speedComponent.Speed;

                direction = new Vector3(movement.ReadValue<Vector2>().x, 0, movement.ReadValue<Vector2>().y);

                if (direction.magnitude > 0.1f) {

                    characterController.Move(direction * speed * Time.deltaTime);
                }

            }
        }
    }
}
