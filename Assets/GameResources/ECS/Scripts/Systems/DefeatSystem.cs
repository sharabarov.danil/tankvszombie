﻿using Leopotam.Ecs;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Defeat System
    /// </summary>
    public class DefeatSystem : IEcsRunSystem {

        private readonly EcsFilter<EndGameComponent> _endGameFilter;
        private readonly EcsFilter<PlayerTagComponent, HealthComponent> _filters;
        public void Run() {
            foreach (var i in _endGameFilter) {
                ref var entity = ref _endGameFilter.GetEntity(i);
                ref var endGameComponent = ref _endGameFilter.Get1(i);
                if (_filters.GetEntitiesCount() == 0) {
                    endGameComponent.View.ShowStatus(false);
                    entity.Destroy();
                }
            }
        }

    }
}