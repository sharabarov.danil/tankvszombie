using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// TargetSearcherSystem
    /// </summary>
    public class TargetSearcherSystem : IEcsRunSystem {

        public enum CharacterState {
            Attack,
            Move,
            Idle,
            Death
        }

        private CharacterState _characterState = CharacterState.Idle;

        private readonly EcsFilter<EnemyTagComponent, ModelComponent, SearchComponent, NavMeshComponent, AnimatorComponent>.Exclude<DeathEvent> _filters;

        public void Run() {
            foreach (var i in _filters) {
                ref var modelComponent = ref _filters.Get2(i);
                ref var searchComponent = ref _filters.Get3(i);
                ref var navMeshComponent = ref _filters.Get4(i);
                ref var animatorComponent = ref _filters.Get5(i);

                Collider[] hitColliders = Physics.OverlapSphere(modelComponent.ModelTransform.position, searchComponent.DistanceLimits.y, searchComponent.TargetLayer);

                if (hitColliders.Length > 0) {
                    foreach (var collider in hitColliders) {
                        if (collider != null && navMeshComponent.NavMeshAgent.isOnNavMesh) {
                            navMeshComponent.NavMeshAgent.isStopped = false;
                            navMeshComponent.NavMeshAgent.SetDestination(collider.transform.position);

                            float distance = navMeshComponent.NavMeshAgent.remainingDistance;
                            _characterState = distance < searchComponent.DistanceLimits.x ? CharacterState.Attack : CharacterState.Move;
                            animatorComponent.Animator.SetInteger("DistanceType", (int)_characterState);
                            animatorComponent.Animator.SetFloat("Speed", navMeshComponent.NavMeshAgent.speed);

                            break;
                        }
                    }
                } else {
                    _characterState = CharacterState.Idle;
                    animatorComponent.Animator.SetInteger("DistanceType", (int)_characterState);

                    if (navMeshComponent.NavMeshAgent.isOnNavMesh) {
                        navMeshComponent.NavMeshAgent.isStopped = true;
                    }
                }
            }
        }
    }
}
