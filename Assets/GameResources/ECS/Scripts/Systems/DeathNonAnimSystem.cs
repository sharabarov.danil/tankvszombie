using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Death Non Anim System
    /// </summary>
    public class DeathNonAnimSystem : IEcsRunSystem {

        private Pool _pool;

        private readonly EcsFilter<ModelComponent, DeathEvent>.Exclude<AnimatorComponent> _filter;

        public void Run() {
            foreach (var i in _filter) {
                ref var entity = ref _filter.GetEntity(i);
                ref var modelComponent = ref _filter.Get1(i);

                _pool.Despawn(modelComponent.ModelTransform.gameObject);
                entity.Destroy();

            }
        }
    }
}
