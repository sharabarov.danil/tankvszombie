﻿using Leopotam.Ecs;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Room Creator
    /// </summary>

    public class RoomSystem : IEcsInitSystem {

        private EcsWorld _world;

        private RoomData _roomData;

        private RoomView _startingRoom;

        private RoomView[,] spawnedRooms;

        private Pool _pool;

        private float _center;

        private void CreateFirstRoom() {

            GameObject roomPrefab = _roomData.RoomPrefab.gameObject;
            _startingRoom = _pool.Spawn(roomPrefab).GetComponent<RoomView>();
            _startingRoom.transform.position = Vector3.zero;
            _startingRoom.ContainPlayer = true;
        }

        private void PlaceOneRoom() {
            HashSet<Vector2Int> vacantPlaces = new HashSet<Vector2Int>();
            for (int x = 0; x < spawnedRooms.GetLength(0); x++) {
                for (int y = 0; y < spawnedRooms.GetLength(1); y++) {
                    if (spawnedRooms[x, y] == null) continue;

                    int maxX = spawnedRooms.GetLength(0) - 1;
                    int maxY = spawnedRooms.GetLength(1) - 1;

                    if (x > 0 && spawnedRooms[x - 1, y] == null) vacantPlaces.Add(new Vector2Int(x - 1, y));
                    if (y > 0 && spawnedRooms[x, y - 1] == null) vacantPlaces.Add(new Vector2Int(x, y - 1));
                    if (x < maxX && spawnedRooms[x + 1, y] == null) vacantPlaces.Add(new Vector2Int(x + 1, y));
                    if (y < maxY && spawnedRooms[x, y + 1] == null) vacantPlaces.Add(new Vector2Int(x, y + 1));
                }
            }

            GameObject roomPrefab = _roomData.RoomPrefab.gameObject;

            RoomView newRoom = _pool.Spawn(roomPrefab).GetComponent<RoomView>();

            int limit = 500;
            while (limit-- > 0) {
                Vector2Int position = vacantPlaces.ElementAt(UnityEngine.Random.Range(0, vacantPlaces.Count));

                if (ConnectToSomething(newRoom, position)) {

                    newRoom.transform.position = new Vector3(position.x - _center, 0, position.y - _center) * newRoom.Floor.transform.localScale.x;
                    spawnedRooms[position.x, position.y] = newRoom;
                    return;
                }
            }

            _pool.Despawn(newRoom.gameObject);
        }

        private bool ConnectToSomething(RoomView room, Vector2Int p) {
            int maxX = spawnedRooms.GetLength(0) - 1;
            int maxY = spawnedRooms.GetLength(1) - 1;

            List<Vector2Int> neighbours = new List<Vector2Int>();

            if (room.CloseDoorU != null && p.y < maxY && spawnedRooms[p.x, p.y + 1]?.CloseDoorD != null) neighbours.Add(Vector2Int.up);
            if (room.CloseDoorD != null && p.y > 0 && spawnedRooms[p.x, p.y - 1]?.CloseDoorU != null) neighbours.Add(Vector2Int.down);
            if (room.CloseDoorR != null && p.x < maxX && spawnedRooms[p.x + 1, p.y]?.CloseDoorL != null) neighbours.Add(Vector2Int.right);
            if (room.CloseDoorL != null && p.x > 0 && spawnedRooms[p.x - 1, p.y]?.CloseDoorR != null) neighbours.Add(Vector2Int.left);

            if (neighbours.Count == 0) return false;

            Vector2Int selectedDirection = neighbours[UnityEngine.Random.Range(0, neighbours.Count)];
            RoomView selectedRoom = spawnedRooms[p.x + selectedDirection.x, p.y + selectedDirection.y];

            if (selectedDirection == Vector2Int.up) {
                room.ActiveUpDoor(false);
                selectedRoom.ActiveDownDoor(false);
            } else if (selectedDirection == Vector2Int.down) {
                room.ActiveDownDoor(false);
                selectedRoom.ActiveUpDoor(false);
            } else if (selectedDirection == Vector2Int.right) {
                room.ActiveRightDoor(false);
                selectedRoom.ActiveLeftDoor(false);
            } else if (selectedDirection == Vector2Int.left) {
                room.ActiveLeftDoor(false);
                selectedRoom.ActiveRightDoor(false);
            }

            return true;
        }

        public void Init() {
            CreateFirstRoom();

            _center = Mathf.Clamp(_roomData.Center, 1, _roomData.RoomCount);
            spawnedRooms = new RoomView[_roomData.RoomCount + 1, _roomData.RoomCount + 1];
            spawnedRooms[_roomData.Center, _roomData.Center] = _startingRoom;

            for (int i = 0; i < _roomData.RoomCount - 1; i++) {
                PlaceOneRoom();
            }

            var entity = _world.NewEntity();
            ref var roomsComponents = ref entity.Get<RoomsComponent>();
            roomsComponents.RoomViews = spawnedRooms;
        }
    }
}