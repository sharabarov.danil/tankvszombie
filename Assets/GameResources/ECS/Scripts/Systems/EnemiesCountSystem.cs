using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Enemies Count System
    /// </summary>
    public class EnemiesCountSystem : IEcsRunSystem {

        private readonly EcsFilter<EnemiesCountComponent> _enemiesViewfilters;
        private readonly EcsFilter<EnemyTagComponent, HealthComponent> _filters;

        public void Run() {
            foreach (var i in _enemiesViewfilters) {
                ref var enemiesNumberComponent = ref _enemiesViewfilters.Get1(i);
                enemiesNumberComponent.View.ShowEnemiesNumber(_filters.GetEntitiesCount());
            }
        }
    }
}
