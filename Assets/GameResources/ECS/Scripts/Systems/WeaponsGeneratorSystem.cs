using Leopotam.Ecs;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Weapon Generator System
    /// </summary>
    public class WeaponsGeneratorSystem : IEcsInitSystem {

        private EcsWorld _world;
        private PlayerData _playerData;
        private readonly EcsFilter<PlayerTagComponent> _tagFilter;

        public void Init() {
            InitWeapons();
        }

        private void InitWeapons() {

            var weapon = _world.NewEntity();

            ref var turningComponent = ref weapon.Get<TurningComponent>();
            turningComponent.TurnSpeed = _playerData.WeaponTurnSpeed;

            ref var mouseLookComponent = ref weapon.Get<MousePositionComponent>();

            ref var inputComponent = ref weapon.Get<InputComponent>();

            ref var directionComponent = ref weapon.Get<DirectionComponent>();

            ref var modelComponent = ref weapon.Get<ModelComponent>();
            Transform weaponPlace = null;

            foreach (var i in _tagFilter) {
                ref var entity = ref _tagFilter.GetEntity(i);
                ref var target = ref entity.Get<WeaponModelComponent>();
                ref var inputActions = ref entity.Get<InputComponent>();

                weaponPlace = target.ModelTransform;
                inputComponent.Actions = inputActions.Actions;
                directionComponent.Direction = inputComponent.Actions.Look;
                mouseLookComponent.Direction = inputComponent.Actions.MousePosition;
            }

            modelComponent.ModelTransform = weaponPlace;
        }

    }
}
