using Leopotam.Ecs;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Enemy Generator
    /// </summary>
    public class EnemyGeneratorSystem : IEcsInitSystem {

        private HealthBarSourceView _healthBarSourceView;
        private Pool _pool;
        private EnemiesRoomData _enemiesRoomData;
        private EcsWorld _world;

        private readonly EcsFilter<RoomsComponent> _filters;

        public static int CurrentEnemiesNumbers = 0;

        public void Generate(RoomView[,] rooms) {
            foreach (var item in rooms) {
                if (item != null) {
                    int rand = item.ContainPlayer ? 0 : UnityEngine.Random.Range(_enemiesRoomData.GenerateOnStartInRoom.x, _enemiesRoomData.GenerateOnStartInRoom.y);
                    for (int i = 0; i < rand; i++) {
                        CreateCharacter(RandomPlace(item));
                    }
                }
            }
        }

        /// <summary>
        /// Create character
        /// </summary>
        /// <param name="place"></param>

        private void CreateCharacter(Vector3 place) {
            EnemyData enemyData = _enemiesRoomData.EnemyDatas[Random.Range(0, _enemiesRoomData.EnemyDatas.Length)];
            GameObject spawnedEnemy = _pool.Spawn(enemyData.EnemyPrefab, place, Quaternion.identity);
            var enemy = _world.NewEntity();

            ref var enemyTagComponent = ref enemy.Get<EnemyTagComponent>();

            ref var modelComponent = ref enemy.Get<ModelComponent>();
            modelComponent.ModelTransform = spawnedEnemy.transform;

            ref var searchComponent = ref enemy.Get<SearchComponent>();
            searchComponent.DistanceLimits = enemyData.DistanceLimits;
            searchComponent.TargetLayer = enemyData.TargetLayer;

            ref var navMeshComponent = ref enemy.Get<NavMeshComponent>();
            if (spawnedEnemy.GetComponent<NavMeshAgent>() != null) {
                navMeshComponent.NavMeshAgent = spawnedEnemy.GetComponent<NavMeshAgent>();
                navMeshComponent.NavMeshAgent.speed = enemyData.Speed;
                navMeshComponent.NavMeshAgent.angularSpeed = enemyData.TurnSpeed;
                navMeshComponent.NavMeshAgent.stoppingDistance = enemyData.DistanceLimits.x;
            }

            ref var animatorComponent = ref enemy.Get<AnimatorComponent>();
            animatorComponent.Animator = spawnedEnemy.GetComponent<Animator>();

            ref var healthComponent = ref enemy.Get<HealthComponent>();
            healthComponent.CurrentHealth = enemyData.Health;
            healthComponent.MaxHealth = enemyData.Health;

            ref var damageComponent = ref enemy.Get<DamageComponent>();
            damageComponent.Damage = enemyData.Damage;

            ref var layerMaskComponent = ref enemy.Get<TriggerLayerComponent>();
            layerMaskComponent.IgnoreLayer = enemyData.IgnoreLayer;


            ref var entityRefComponent = ref enemy.Get<EntityRefComponent>();
            if (spawnedEnemy.GetComponent<EntityView>() != null) {
                entityRefComponent.EntityRef = spawnedEnemy.GetComponent<EntityView>();
                entityRefComponent.EntityRef.Entity = enemy;
            }

            ref var triggerCheckerComponent = ref enemy.Get<TriggerCheckerComponent>();
            if (spawnedEnemy.GetComponent<AnimAttackDealer>() != null) {
                triggerCheckerComponent.TriggerChecker = spawnedEnemy.GetComponent<AnimAttackDealer>();
                triggerCheckerComponent.TriggerChecker.Init(_world, _pool, enemy);
            }

            ref var deathCheckerComponent = ref enemy.Get<DeathCheckerComponent>();
            if (spawnedEnemy.GetComponent<AnimDeathDealer>() != null) {
                deathCheckerComponent.AnimDeathChecker = spawnedEnemy.GetComponent<AnimDeathDealer>();
                deathCheckerComponent.AnimDeathChecker.Init(_pool, enemy);
            }

            var spawnedHealthBar = _pool.Spawn(enemyData.HealthBar.gameObject, _healthBarSourceView.SourceView);

            var healthBarEntity = _world.NewEntity();

            ref var healthBarComponent = ref healthBarEntity.Get<HealthBarComponent>();
            healthBarComponent.HealthBar = spawnedHealthBar.GetComponent<Image>();
            healthBarComponent.HealthTarget = enemy;

            CurrentEnemiesNumbers++;
        }

        /// <summary>
        /// Random Place
        /// </summary>

        private Vector3 RandomPlace(RoomView room) {

            float randomX = Random.Range(-_enemiesRoomData.Radius, _enemiesRoomData.Radius);
            float randomZ = Random.Range(-_enemiesRoomData.Radius, _enemiesRoomData.Radius);
            Vector3 random = new Vector3(randomX, 0, randomZ);

            Vector3 targetPos = room.transform.position + random + Vector3.down;
            RaycastHit physicsHit;
            if (Physics.Raycast(room.transform.position + Vector3.up, targetPos - room.transform.position, out physicsHit)) {
                //Debug.Log(physicsHit.distance);
                //return physicsHit.point;
                NavMeshHit navmeshHit;
                if (NavMesh.SamplePosition(physicsHit.point, out navmeshHit, 1.0f, NavMesh.AllAreas)) {
                    Debug.Log(navmeshHit.distance);
                    return navmeshHit.position;
                }
            }

            return room.transform.position + random;
        }

        public void Init() {
            foreach (var i in _filters) {
                ref var roomsComponent = ref _filters.Get1(i);
                Generate(roomsComponent.RoomViews);
            }
        }
    }
}
