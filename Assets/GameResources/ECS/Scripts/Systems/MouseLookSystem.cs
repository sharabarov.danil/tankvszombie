﻿using Leopotam.Ecs;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Look System
    /// </summary>
    sealed class MouseLookSystem : IEcsInitSystem, IEcsDestroySystem {

        private readonly EcsFilter<PlayerTagComponent> _playerTagFilter;
        private readonly EcsFilter<CameraComponent> _cameraFilter = null;
        private readonly EcsFilter<ModelComponent, TurningComponent, MousePositionComponent> turningFilter = null;
        private CancellationTokenSource _cancelationTokenSource;
        private InputAction _mouseClickAction;
        private Vector3 direction;
        private Vector3 mouseDirection;

        public void Destroy() {
            EndFiring();

            _mouseClickAction.started -= _ => StartFiring();
            _mouseClickAction.canceled -= _ => EndFiring();
        }

        public void Init() {
            foreach (var i in _playerTagFilter) {
                ref var entity = ref _playerTagFilter.GetEntity(i);
                ref var inputComponent = ref entity.Get<InputComponent>();

                _mouseClickAction = inputComponent.Actions.MouseClick;
            }

            _mouseClickAction.started += _ => StartFiring();
            _mouseClickAction.canceled += _ => EndFiring();
        }

        private async void StartFiring() {
            _cancelationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = _cancelationTokenSource.Token;
            while (!cancellationToken.IsCancellationRequested) {
                Fire();
                await Task.Yield();
            }
        }

        private void Fire() {
            foreach (var j in _cameraFilter) {
                ref var cameraComponent = ref _cameraFilter.Get1(j);
                foreach (var i in turningFilter) {
                    ref var modelComponent = ref turningFilter.Get1(i);
                    ref var turningComponent = ref turningFilter.Get2(i);
                    ref var directionComponent = ref turningFilter.Get3(i);

                    ref var directionValue = ref directionComponent.Direction;
                    ref var transform = ref modelComponent.ModelTransform;
                    ref var turnSpeed = ref turningComponent.TurnSpeed;

                    mouseDirection = (cameraComponent.FollowCamera.ScreenToViewportPoint(directionValue.ReadValue<Vector2>()) - new Vector3(0.5f, 0.5f, 0)) * 2;

                    direction = new Vector3(mouseDirection.x, 0, mouseDirection.y);

                    if (direction.magnitude > 0.1f) {

                        var rotation = Quaternion.LookRotation(direction);

                        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * turnSpeed);
                    }

                }
            }
        }

        private void EndFiring() {
            if (_cancelationTokenSource != null) {
                _cancelationTokenSource.Cancel();
                _cancelationTokenSource = null;
            }
        }


    }
}
