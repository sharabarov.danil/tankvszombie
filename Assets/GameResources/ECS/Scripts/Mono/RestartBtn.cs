using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Restart Btn
    /// </summary>
    public class RestartBtn : MonoBehaviour, IPointerClickHandler {
        public void OnPointerClick(PointerEventData eventData) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}