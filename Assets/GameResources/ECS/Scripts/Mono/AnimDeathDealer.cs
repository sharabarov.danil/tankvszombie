﻿using Leopotam.Ecs;
using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// AnimDeath Dealer
    /// </summary>
    public class AnimDeathDealer : MonoBehaviour {

        private Pool _pool;
        private EcsEntity _entity;

        public void Init(Pool pool, EcsEntity entity) {
            _pool = pool;
            _entity = entity;
        }


        /// <summary>
        /// Damage Event
        /// </summary>
        public void Death() {
            if (_entity.Has<ModelComponent>()) {
                _pool.Despawn(_entity.Get<ModelComponent>().ModelTransform.gameObject);
                _entity.Destroy();
            }
        }
    }
}
