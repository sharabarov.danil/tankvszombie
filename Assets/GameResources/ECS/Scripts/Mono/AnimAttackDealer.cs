﻿using Leopotam.Ecs;
using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// AnimAttack Dealer
    /// </summary>
    public class AnimAttackDealer : AttackDealer {

        [Header("Radius:")]
        [SerializeField]
        private float _radius = 1f;

        /// <summary>
        /// Damage Event
        /// </summary>
        public void Damage() {

            var entity = _world.NewEntity();

            ref var damageComponent = ref entity.Get<DamageComponent>();
            int damage = _damageSource.Get<DamageComponent>().Damage;
            damageComponent.Damage = damage;

            ref var ignoreLayerComponent = ref entity.Get<TriggerLayerComponent>();
            ignoreLayerComponent.IgnoreLayer = _damageSource.Get<TriggerLayerComponent>().IgnoreLayer;

            LayerMask triggetLayer = _damageSource.Get<SearchComponent>().TargetLayer;

            Collider[] hitColliders = Physics.OverlapSphere(transform.position, _radius, triggetLayer);

            if (hitColliders.Length > 0) {
                Hit(entity, hitColliders[0]);
            }
        }
    }
}
