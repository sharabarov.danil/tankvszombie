﻿using UnityEngine;
using UnityEngine.AI;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Room
    /// </summary>
    public class RoomView : MonoBehaviour {
        [Header("Up door:")]
        public GameObject OpenDoorU;
        public GameObject CloseDoorU;
        [Header("Right door:")]
        public GameObject OpenDoorR;
        public GameObject CloseDoorR;
        [Header("Bottom door:")]
        public GameObject OpenDoorD;
        public GameObject CloseDoorD;
        [Header("Left door:")]
        public GameObject OpenDoorL;
        public GameObject CloseDoorL;

        [Header("Floor:")]
        public GameObject Floor;

        public bool ContainPlayer;

        /// <summary>
        /// Activate up door
        /// </summary>
        /// <param name="status"></param>
        public void ActiveUpDoor(bool status) {
            OpenDoorU.SetActive(!status);
            CloseDoorU.SetActive(status);
        }

        /// <summary>
        /// Activate right door
        /// </summary>
        /// <param name="status"></param>
        public void ActiveRightDoor(bool status) {
            OpenDoorR.SetActive(!status);
            CloseDoorR.SetActive(status);
        }

        /// <summary>
        /// Activate bottom door
        /// </summary>
        /// <param name="status"></param>
        public void ActiveDownDoor(bool status) {
            OpenDoorD.SetActive(!status);
            CloseDoorD.SetActive(status);
        }

        /// <summary>
        /// Activate left door
        /// </summary>
        /// <param name="status"></param>
        public void ActiveLeftDoor(bool status) {
            OpenDoorL.SetActive(!status);
            CloseDoorL.SetActive(status);
        }

    }
}