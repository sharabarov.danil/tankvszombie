using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Attack dealer
    /// </summary>
    public class AttackDealer : MonoBehaviour {

        protected EcsEntity _damageSource;
        protected EcsWorld _world;
        protected Pool _pool;

        /// <summary>
        /// Init
        /// </summary>
        /// <param name="damageSource"></param>
        public void Init(EcsWorld world, Pool pool, EcsEntity damageSource) {
            _world = world;
            _damageSource = damageSource;
            _pool = pool;
        }

        /// <summary>
        /// Hit
        /// </summary>
        /// <param name="other"></param>
        protected void Hit(EcsEntity damageSource, Collider other) {
            var hit = _world.NewEntity();

            ref var attackComponent = ref hit.Get<AttackComponent>();

            attackComponent.MyTriggerEntity = damageSource;

            LayerMask triggetLayer = damageSource.Get<TriggerLayerComponent>().IgnoreLayer;

            if (other.gameObject.layer != triggetLayer.value) {

                EntityView entityRef = other.GetComponent<EntityView>();

                if (entityRef != null) {

                    ref var hitComponent = ref hit.Get<HitComponent>();

                    hitComponent.OtherTriggerEntity = entityRef.Entity;
                } else {
                    if (attackComponent.MyTriggerEntity.IsAlive()) {
                        if (attackComponent.MyTriggerEntity.Has<ModelComponent>()) {
                            ref var modelComponent = ref attackComponent.MyTriggerEntity.Get<ModelComponent>();
                            _pool.Despawn(modelComponent.ModelTransform.gameObject);
                        }

                        attackComponent.MyTriggerEntity.Destroy();
                    }
                }
            }
        }
    }
}
