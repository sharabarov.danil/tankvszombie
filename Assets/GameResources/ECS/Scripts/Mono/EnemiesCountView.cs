using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Enemies Number View
    /// </summary>
    public class EnemiesCountView : MonoBehaviour {
        [SerializeField]
        private Text textView;

        /// <summary>
        /// Show number
        /// </summary>
        /// <param name="text"></param>
        public void ShowEnemiesNumber(int number) {
            textView.text = $"Enemies numbers: {number}";
        }
    }
}
