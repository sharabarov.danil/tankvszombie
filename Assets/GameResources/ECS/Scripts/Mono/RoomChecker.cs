﻿using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Room checker
    /// </summary>
    public class RoomChecker : MonoBehaviour {
        [SerializeField]
        private string _playerTag = "Player";
        [SerializeField]
        private RoomView room;

        private void OnCollisionEnter(Collision collision) {
            if (collision.gameObject.tag == _playerTag) {
                room.ContainPlayer = true;
            }
        }

        private void OnCollisionExit(Collision collision) {
            if (collision.gameObject.tag == _playerTag) {
                room.ContainPlayer = false;
            }
        }

    }
}
