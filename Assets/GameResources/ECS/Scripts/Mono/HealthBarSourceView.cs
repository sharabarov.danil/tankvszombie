﻿using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// HealthBar Source View
    /// </summary>
    public class HealthBarSourceView : MonoBehaviour {
        public Transform SourceView;
    }
}
