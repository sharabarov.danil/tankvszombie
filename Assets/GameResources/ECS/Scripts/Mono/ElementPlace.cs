using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Element Place
    /// </summary>
    public class ElementPlace : MonoBehaviour {
        [SerializeField]
        private Transform _place;

        public Transform Place {
            get {
                return _place;
            }
        }
    }
}
