using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TankVsZombie.ECS {

    /// <summary>
    /// End Game View
    /// </summary>
    public class EndGameView : MonoBehaviour {
        [SerializeField]
        private Text textView;

        private const string VICTORY_TEXT = "VICTORY!";
        private const string DEFEAT_TEXT = "DEFEAT!";

        /// <summary>
        /// Show Status
        /// </summary>
        /// <param name="text"></param>
        public void ShowStatus(bool victory) {
            textView.text = victory ? VICTORY_TEXT : DEFEAT_TEXT;
        }
    }
}