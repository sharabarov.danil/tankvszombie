using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TankVsZombie.ECS {

    /// <summary>
    /// Entity View
    /// </summary>
    public class EntityView : MonoBehaviour {
        public EcsEntity Entity;
    }
}
