using Leopotam.Ecs;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Ecs Game Start Up
    /// </summary>

    public sealed class EcsGameStartup : MonoBehaviour {

        [SerializeField]
        private RoomData _roomData;

        [SerializeField]
        private PlayerData _playerData;

        [SerializeField]
        private EnemiesRoomData _enemesRoomData;

        [SerializeField]
        private CameraData _cameraData;

        [SerializeField]
        private HealthBarSourceView _healthBarSourceView;

        [SerializeField]
        private EnemiesCountView _enemiesCountView;

        [SerializeField]
        private EndGameView _endGameView;

        private EcsWorld _world;
        private EcsSystems _systems;
        private PlayerInputController _playerInputController;

        private void Awake() {
            _playerInputController = new PlayerInputController();
        }

        private void Start() {
            _world = new EcsWorld();
            _systems = new EcsSystems(_world);

            AddSystems();
            AddInjections();
            AddOneFrames();

            _systems.Init();
        }

        private void OnEnable() {
            _playerInputController.Enable();
        }

        private void OnDisable() {
            _playerInputController.Disable();
        }

        private void FixedUpdate() {
            _systems.Run();
        }

        private void AddInjections() {
            _systems.
             Inject(new Pool()).
             Inject(_healthBarSourceView).
             Inject(_roomData).
             Inject(_playerData).
             Inject(_enemesRoomData).
             Inject(_enemiesCountView).
             Inject(_endGameView).
             Inject(_cameraData);
        }

        private void AddSystems() {
            _systems.
                Add(new RoomSystem()).
                Add(new PlayerGeneratorSystem()).
                Add(new EnemyGeneratorSystem()).
                Add(new HitSystem()).
                Add(new TargetSearcherSystem()).
                Add(new WeaponsGeneratorSystem()).
                Add(new WeaponsSwitcherSystem()).
                Add(new WeaponsShootSystem()).
                Add(new TurningSystem()).
                Add(new MouseLookSystem()).
                Add(new MoveSystem()).
                Add(new TranslateSystem()).
                Add(new LifeTimeSystem()).
                Add(new CameraFollowSystem()).
                Add(new HealthBarPositionSystem()).
                Add(new HealthBarSystem()).
                Add(new DeathSystem()).
                Add(new DeathAnimSystem()).
                Add(new DeathNonAnimSystem()).
                Add(new EnemiesCountUISystem()).
                Add(new EnemiesCountSystem()).
                Add(new EndGameInitSystem()).
                 Add(new DefeatSystem()).
                Add(new VictorySystem());

        }

        private void AddOneFrames() {
            _systems.
                OneFrame<DeathEvent>();

        }

        private void OnDestroy() {

            if (_systems == null) return;

            _systems.Destroy();
            _systems = null;
            _world.Destroy();
            _world = null;
        }
    }
}
