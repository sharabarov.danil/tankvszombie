using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Projectile Attack Dealer
    /// </summary>
    public class ProjectileAttackDealer : AttackDealer {

        private void OnTriggerEnter(Collider other) {
            Hit(_damageSource, other);
        }
    }
}
