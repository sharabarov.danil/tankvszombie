﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Object Pool
    /// </summary>

    public class Pool {

        private List<ObjectPool> _objectPools = new List<ObjectPool>();

        private GameObject _currentPoolElement = null;

        /// <summary>
        /// Get element from pool
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="poolID"></param>
        /// <param name="parent"></param>
        /// <returns></returns>

        public GameObject Spawn(GameObject prefab, Transform parent = null) {

            if (prefab != null) {
                if (GetFreePoolElement(prefab.name) != null) {
                    _currentPoolElement = GetFreePoolElement(prefab.name);
                    if (parent != null) {
                        _currentPoolElement.transform.SetParent(parent);
                    }
                } else {
                    Create(prefab, parent);
                    if (GetObjectPool(prefab.name) != null) {
                        GetObjectPool(prefab.name).PoolElementObject.Add(_currentPoolElement);
                    } else {
                        AddElement(prefab.name);
                    }
                }
                _currentPoolElement.SetActive(true);
                return _currentPoolElement;
            }
            return null;
        }

        public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent = null) {

            if (prefab != null) {
                if (GetFreePoolElement(prefab.name) != null) {
                    _currentPoolElement = GetFreePoolElement(prefab.name);
                    if (parent != null) {
                        _currentPoolElement.transform.SetParent(parent);
                    }
                    _currentPoolElement.transform.SetPositionAndRotation(position, rotation);
                } else {
                    Create(prefab, position, rotation, parent);
                    if (GetObjectPool(prefab.name) != null) {
                        GetObjectPool(prefab.name).PoolElementObject.Add(_currentPoolElement);
                    } else {
                        AddElement(prefab.name);
                    }
                }
                _currentPoolElement.SetActive(true);
                return _currentPoolElement;
            }
            return null;
        }

        private void Create(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent = null) {
            if (parent != null) {
                _currentPoolElement = Object.Instantiate(prefab, position, rotation, parent);
            } else {
                _currentPoolElement = Object.Instantiate(prefab, position, rotation);
            }
        }

        private void Create(GameObject prefab, Transform parent = null) {
            if (parent != null) {
                _currentPoolElement = Object.Instantiate(prefab, parent);
            } else {
                _currentPoolElement = Object.Instantiate(prefab);
            }
        }

        private void AddElement(string poolID) {
            ObjectPool objectPool = new ObjectPool {
                poolID = poolID
            };
            objectPool.PoolElementObject.Add(_currentPoolElement);
            _objectPools.Add(objectPool);
        }

        /// <summary>
        /// Find free element
        /// </summary>
        /// <param name="poolID"></param>
        /// <returns></returns>
        private GameObject GetFreePoolElement(string poolID) {
            if (GetObjectPool(poolID) == null) {
                return null;
            }

            return GetObjectPool(poolID).PoolElementObject.Find(x => !x.activeInHierarchy);
        }

        /// <summary>
        /// Remove pool element
        /// </summary>
        /// <param name="poolElement"></param>
        public void Despawn(GameObject poolElement) {
            if (poolElement != null) {
                poolElement.SetActive(false);
            }
        }

        /// <summary>
        /// Remove all elemets
        /// </summary>
        public void DespawnAll() {
            for (int i = 0; i < _objectPools.Count; i++) {
                for (int j = 0; j < _objectPools[i].PoolElementObject.Count; j++) {
                    Despawn(_objectPools[i].PoolElementObject[j].gameObject);
                }
            }
        }

        /// <summary>
        /// Get pool element with poolID
        /// </summary>
        /// <param name="poolID"></param>
        /// <returns></returns>
        private ObjectPool GetObjectPool(string poolID) {
            return _objectPools.Find(x => x.poolID == poolID);
        }

        public class ObjectPool {
            public string poolID = string.Empty;
            public List<GameObject> PoolElementObject = new List<GameObject>();
        }
    }
}
