using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Health Component
    /// </summary>
    internal struct HealthComponent {
        public int CurrentHealth;
        public int MaxHealth;
    }
}
