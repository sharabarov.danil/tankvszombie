﻿using TankVsZombie.NoECS;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Bullet Component
    /// </summary>
    internal struct BulletComponent {

        public float Speed;

        public float LifeTime;

        public DamageData DamageData;

        public BulletData BulletData;
    }
}
