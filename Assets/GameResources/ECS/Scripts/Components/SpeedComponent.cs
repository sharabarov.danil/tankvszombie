﻿
using System;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Speed Component
    /// </summary>
    internal struct SpeedComponent {
        public float Speed;
    }
}
