﻿using Leopotam.Ecs;
using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Attack Component
    /// </summary>
    internal struct AttackComponent {
        public EcsEntity MyTriggerEntity;
    }
}
