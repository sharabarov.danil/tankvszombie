﻿using UnityEngine.InputSystem;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Mouse Position Component
    /// </summary>
    internal struct MousePositionComponent {
        public InputAction Direction;
    }
}
