using Leopotam.Ecs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Health Bar Component
    /// </summary>
    internal struct HealthBarComponent {
        public EcsEntity HealthTarget;
        public Image HealthBar;
    }
}
