﻿namespace TankVsZombie.ECS {
    /// <summary>
    /// Entity Ref Component
    /// </summary>
    internal struct EntityRefComponent {
        public EntityView EntityRef;
    }
}
