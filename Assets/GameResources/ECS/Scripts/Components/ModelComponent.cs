﻿using System;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Model Component
    /// </summary>
    internal struct ModelComponent {
        public Transform ModelTransform;
    }
}
