﻿using Leopotam.Ecs;
using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Hit Component
    /// </summary>
    internal struct HitComponent {
        public EcsEntity OtherTriggerEntity;
    }
}
