
using System;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Character Controller Component
    /// </summary>
    internal struct CharacterControllerComponent {
        public CharacterController CharacterController;
    }
}
