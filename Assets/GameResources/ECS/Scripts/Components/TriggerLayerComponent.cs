﻿using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// TriggerLayer Component
    /// </summary>
    internal struct TriggerLayerComponent {
        public LayerMask IgnoreLayer;
    }
}
