﻿using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Search Component
    /// </summary>
    internal struct SearchComponent {
        public LayerMask TargetLayer;
        public Vector2 DistanceLimits;
    }
}
