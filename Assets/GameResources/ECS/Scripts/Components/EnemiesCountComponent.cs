﻿namespace TankVsZombie.ECS {
    /// <summary>
    /// EnemiesNumber Component
    /// </summary>
    internal struct EnemiesCountComponent {
        public EnemiesCountView View;
    }
}
