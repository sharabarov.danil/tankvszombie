﻿using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Weapon Model Component
    /// </summary>
    internal struct WeaponModelComponent {
        public Transform ModelTransform;
    }
}
