﻿using System;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Camera Settings Component
    /// </summary>
    internal struct CameraMoveComponent {
        public float FollowDistance;
        public float FollowHeight;
        public bool SmoothedFollow;
        public float SmoothSpeed;
    }
}
