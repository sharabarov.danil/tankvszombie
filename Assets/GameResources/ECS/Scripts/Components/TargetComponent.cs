﻿using System;
using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Target Component
    /// </summary>
    internal struct TargetComponent {
        public Transform ModelTransform;
    }
}
