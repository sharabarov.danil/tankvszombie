using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Weapon Component
    /// </summary>
    internal struct WeaponComponent {
        public int Frequency;
        public BulletData BulletData;
    }
}
