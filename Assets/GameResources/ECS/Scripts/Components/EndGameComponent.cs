﻿namespace TankVsZombie.ECS {
    /// <summary>
    /// EndGame Component
    /// </summary>
    internal struct EndGameComponent {
        public EndGameView View;
    }
}
