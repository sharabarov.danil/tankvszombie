
using System;
using TankVsZombie.NoECS;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Turning Component
    /// </summary>
    internal struct TurningComponent {
        public float TurnSpeed;
    }
}
