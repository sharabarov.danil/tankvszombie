﻿
using System;

namespace TankVsZombie.ECS {
    /// <summary>
    /// LifeTime Component
    /// </summary>
    internal struct LifeTimeComponent {
        public float LifeTime;
    }
}
