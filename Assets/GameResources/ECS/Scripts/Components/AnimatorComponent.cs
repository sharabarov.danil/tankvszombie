﻿using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Animator Component
    /// </summary>
    internal struct AnimatorComponent {
        public Animator Animator;
    }
}
