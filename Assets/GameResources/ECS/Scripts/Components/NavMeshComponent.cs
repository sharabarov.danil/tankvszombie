﻿using UnityEngine.AI;

namespace TankVsZombie.ECS {
    /// <summary>
    /// NavMesh Component
    /// </summary>
    internal struct NavMeshComponent {
        public NavMeshAgent NavMeshAgent;
    }
}
