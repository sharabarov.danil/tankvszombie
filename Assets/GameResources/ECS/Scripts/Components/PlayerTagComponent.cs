using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leopotam.Ecs;

namespace TankVsZombie.ECS {

    /// <summary>
    /// PlayerTag Component
    /// </summary>
    internal struct PlayerTagComponent : IEcsIgnoreInFilter {
    }
}
