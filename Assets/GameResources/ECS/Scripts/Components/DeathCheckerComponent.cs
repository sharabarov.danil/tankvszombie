﻿namespace TankVsZombie.ECS {
    /// <summary>
    /// Death Checker Component
    /// </summary>
    internal struct DeathCheckerComponent {
        public AnimDeathDealer AnimDeathChecker;
    }
}
