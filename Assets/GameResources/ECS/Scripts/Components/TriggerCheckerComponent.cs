﻿namespace TankVsZombie.ECS {
    /// <summary>
    /// Trigger Checker Component
    /// </summary>
    internal struct TriggerCheckerComponent {
        public AttackDealer TriggerChecker;
    }
}
