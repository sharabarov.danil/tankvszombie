﻿using System;
using UnityEngine;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Camera Component
    /// </summary>
    internal struct CameraComponent {
        public Camera FollowCamera;
    }
}
