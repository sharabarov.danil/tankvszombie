﻿namespace TankVsZombie.ECS {
    /// <summary>
    /// Damage Component
    /// </summary>
    internal struct DamageComponent {
        public int Damage;
    }
}
