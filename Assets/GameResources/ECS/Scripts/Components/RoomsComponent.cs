﻿namespace TankVsZombie.ECS {
    /// <summary>
    /// Rooms Component
    /// </summary>
    internal struct RoomsComponent {
        public RoomView[,] RoomViews;
    }
}