﻿using System;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Input Component
    /// </summary>
    internal struct InputComponent {
        public PlayerInputController.PlayerActions Actions;
    }
}
