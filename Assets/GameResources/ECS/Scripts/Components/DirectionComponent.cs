﻿
using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TankVsZombie.ECS {
    /// <summary>
    /// Direction Component
    /// </summary>
    internal struct DirectionComponent {
        public InputAction Direction;
    }
}
