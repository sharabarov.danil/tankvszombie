﻿using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Bullet data
    /// </summary>
    [CreateAssetMenu(fileName = "EnemiesRoomData", menuName = "GameData/ECS/Create EnemiesRoomData")]
    public class EnemiesRoomData : ScriptableObject {

        [Header("Enemies's Data")]
        [SerializeField]
        private EnemyData[] _enemyDatas = new EnemyData[0];

        public EnemyData[] EnemyDatas {
            get {
                return _enemyDatas;
            }
        }

        [Header("Character's count on start:")]
        [SerializeField]
        private Vector2Int _generateOnStartInRoom = new Vector2Int(0, 6);

        public Vector2Int GenerateOnStartInRoom {
            get {
                return _generateOnStartInRoom;
            }
        }

        [Header("Radius for character's creation:")]
        [SerializeField]
        private float _radius = 1.0f;

        public float Radius {
            get {
                return _radius;
            }
        }
    }
}
