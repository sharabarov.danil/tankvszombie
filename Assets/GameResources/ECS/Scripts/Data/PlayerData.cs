using System.Collections;
using System.Collections.Generic;
using TankVsZombie.NoECS;
using UnityEngine;
using UnityEngine.UI;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Player Data
    /// </summary>
    [CreateAssetMenu(fileName = "PlayerData", menuName = "GameData/ECS/Create PlayerData")]
    public class PlayerData : ScriptableObject {

        [Header("Player:")]
        [SerializeField]
        private GameObject _playerPrefab;

        public GameObject PlayerPrefab {
            get {
                return _playerPrefab;
            }
        }

        [Header("Speed:")]
        [SerializeField]
        private float _speed;

        public float Speed {
            get {
                return _speed;
            }
        }

        [Header("Turn Speed:")]
        [SerializeField]
        private float _turnSpeed;

        public float TurnSpeed {
            get {
                return _turnSpeed;
            }
        }

        [Header("Health Bar:")]
        [SerializeField]
        private Image _healthBar;

        public Image HealthBar {
            get {
                return _healthBar;
            }
        }

        [Header("Health:")]
        [SerializeField]
        private int _health = 100;

        public int Health {
            get {
                return _health;
            }
        }

        [Header("Weapon Turn Speed:")]
        [SerializeField]
        private float _weaponTurnSpeed;

        public float WeaponTurnSpeed {
            get {
                return _weaponTurnSpeed;
            }
        }

        [Header("Weapons:")]
        [SerializeField]
        private WeaponData[] _weapons;

        public WeaponData[] Weapons {
            get {
                return _weapons;
            }
        }
    }
}
