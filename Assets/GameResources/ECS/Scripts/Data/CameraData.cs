using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Camera Data
    /// </summary>
    [CreateAssetMenu(fileName = "CameraData", menuName = "GameData/ECS/Create CameraData")]
    public class CameraData : ScriptableObject {

        [SerializeField]
        private Camera _followCamera;

        public Camera FollowCamera {
            get {
                return _followCamera;
            }
        }

        [SerializeField]
        private float _followDistance;

        public float FollowDistance {
            get {
                return _followDistance;
            }
        }

        [SerializeField]
        private float _followHeight;

        public float FollowHeight {
            get {
                return _followHeight;
            }
        }

        [SerializeField]
        private bool _smoothedFollow;

        public bool SmoothedFollow {
            get {
                return _smoothedFollow;
            }
        }

        [SerializeField]
        private float _smoothSpeed;

        public float SmoothSpeed {
            get {
                return _smoothSpeed;
            }
        }
    }
}
