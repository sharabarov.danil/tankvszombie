using UnityEngine;
using UnityEngine.UI;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Enemy Data
    /// </summary>
    [CreateAssetMenu(fileName = "EnemyData", menuName = "GameData/ECS/Create EnemyData")]
    public class EnemyData : ScriptableObject {

        [Header("Enemy:")]
        [SerializeField]
        private GameObject _enemyPrefab;

        public GameObject EnemyPrefab {
            get {
                return _enemyPrefab;
            }
        }

        [Header("Target Layer:")]
        [SerializeField]
        private LayerMask _targetLayer;

        public LayerMask TargetLayer {
            get {
                return _targetLayer;
            }
        }

        [Header("Ignore Layer:")]
        [SerializeField]
        private LayerMask _ignoreLayer;

        public LayerMask IgnoreLayer {
            get {
                return _ignoreLayer;
            }
        }

        [Header("DistanceLimits:")]
        [SerializeField]
        private Vector2 _distanceLimits;

        public Vector2 DistanceLimits {
            get {
                return _distanceLimits;
            }
        }

        [Header("Speed:")]
        [SerializeField]
        private float _speed;

        public float Speed {
            get {
                return _speed;
            }
        }

        [Header("Turn Speed:")]
        [SerializeField]
        private float _turnSpeed;

        public float TurnSpeed {
            get {
                return _turnSpeed;
            }
        }

        [Header("Health Bar:")]
        [SerializeField]
        private Image _healthBar;

        public Image HealthBar {
            get {
                return _healthBar;
            }
        }

        [Header("Health:")]
        [SerializeField]
        private int _health = 100;

        public int Health {
            get {
                return _health;
            }
        }

        [Header("Damage:")]
        [SerializeField]
        private int _damage = 30;

        public int Damage {
            get {
                return _damage;
            }
        }
    }
}
