﻿using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Bullet data
    /// </summary>
    [CreateAssetMenu(fileName = "RoomData", menuName = "GameData/ECS/Create RoomData")]
    public class RoomData : ScriptableObject {
        [Header("Rooms count:")]
        [Range(1, 50)]
        [SerializeField]
        private int _roomCount = 12;

        public int RoomCount {
            get {
                return _roomCount;
            }
        }

        [Header("Central room:")]
        [SerializeField]
        private int _center = 5;

        public int Center {
            get {
                return _center;
            }
        }

        [Header("Room prefab")]
        [SerializeField]
        private RoomView _roomPrefab;

        public RoomView RoomPrefab {
            get {
                return _roomPrefab;
            }
        }
    }
}
