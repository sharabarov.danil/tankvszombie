﻿using TankVsZombie.NoECS;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Bullet data
    /// </summary>
    [CreateAssetMenu(fileName = "BulletData", menuName = "GameData/ECS/Create BulletData")]
    public class BulletData : ScriptableObject {

        [Header("Bullet Prefab:")]
        [SerializeField]
        private GameObject _bullet;

        public GameObject Bullet {
            get {
                return _bullet;
            }
        }

        [Header("Ignore Layer:")]
        [SerializeField]
        private LayerMask _ignoreLayer;

        public LayerMask IgnoreLayer {
            get {
                return _ignoreLayer;
            }
        }

        [Header("Speed:")]
        [SerializeField]
        private float _speed = 30.0f;

        public float Speed {
            get {
                return _speed;
            }
        }

        [Header("Life time:")]
        [SerializeField]
        private float _lifeTime = 1f;

        public float LifeTime {
            get {
                return _lifeTime;
            }
        }

        [Header("Damage Data:")]
        [SerializeField]
        private int _damage = 30;

        public int Damage {
            get {
                return _damage;
            }
        }
    }
}
