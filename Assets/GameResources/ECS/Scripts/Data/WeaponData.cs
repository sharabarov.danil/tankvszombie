using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankVsZombie.ECS {

    /// <summary>
    /// Player Data
    /// </summary>
    [CreateAssetMenu(fileName = "WeaponData", menuName = "GameData/ECS/Create WeaponData")]
    public class WeaponData : ScriptableObject {

        [Header("Weapon:")]
        [SerializeField]
        private GameObject _weaponPrefab;

        public GameObject WeaponPrefab {
            get {
                return _weaponPrefab;
            }
        }

        [Header("Frequency:")]
        [SerializeField]
        private int _frequency = 300;

        public int Frequency {
            get {
                return _frequency;
            }
        }

        [Header("Bullet Data:")]
        [SerializeField]
        private BulletData _bulletData;

        public BulletData BulletData {
            get {
                return _bulletData;
            }
        }

    }
}
