# TankVsZombies

**Sample project for showing difference OOP and DOP**

This project is an example of the top-down shooter. The game area consists of predefined number of 3D rooms. All rooms are generated automatically creating random labyrinth which depends on the set of paramaters (rooms number, center of level). The main character is a tank which is generated in one of created rooms. The tank has two types of weapons which has different characteristics (damage, frequency and etc.). Enemies (Zombies) are generated in other rooms and has different characteristic too. They start movement when the tank arrives in their zone of accessibility. The goal of the game is cleaning all rooms from enemies, while enemies can damage and destroy the tank. 

Stack of technologies:
1. LeoEcs
2. New Input System
3. NavMesh
4. Unity Physics
5. Unity Animator
6. Pool
